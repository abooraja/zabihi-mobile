package com.bs.ecommerce.model;

import com.bs.ecommerce.networking.BaseResponse;

import java.util.List;

/**
 * Created by bs-110 on 1/7/2016.
 */
public class CategoryFeaturedProductAndSubcategoryResponse extends BaseResponse{
    String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    private List<Category> SubCategories;
    private List<ProductModel> FeaturedProducts;

    public List<ProductModel> getFeaturedProducts() {
        return FeaturedProducts;
    }

    public void setFeaturedProducts(List<ProductModel> featuredProducts) {
        FeaturedProducts = featuredProducts;
    }

    public List<Category> getSubCategories() {
        return SubCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        SubCategories = subCategories;
    }
}
