package com.bs.ecommerce.model;

import com.bs.ecommerce.networking.BaseResponse;

/**
 * .
 */
public class AddAllItemsToCartFromWishlistResponse extends BaseResponse {
    int Count;
    int productId;
    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }



    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }
}
