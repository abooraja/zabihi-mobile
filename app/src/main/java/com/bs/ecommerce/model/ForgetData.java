package com.bs.ecommerce.model;

/**
 * Created by BS-182 on 5/24/2017.
 */

public class ForgetData {

    private String Email;

    public ForgetData(String  email){
        Email=email;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
