package com.bs.ecommerce.model;

/**
 * .
 */
public class ViewType {
    public static final int GRID = 0;
    public static final int LIST = 1;

    public static final int SINGLE = 2;
    public static final int HOMEPAGE_VIEW = 3;
    public static final int PROGRESS = 4;


}
