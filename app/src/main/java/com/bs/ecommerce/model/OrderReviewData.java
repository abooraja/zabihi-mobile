package com.bs.ecommerce.model;

/**
 * .
 */
public class OrderReviewData {
    private com.bs.ecommerce.model.BillingAddress BillingAddress;

    private com.bs.ecommerce.model.BillingAddress ShippingAddress;
    private com.bs.ecommerce.model.BillingAddress PickupAddress;
    private String ShippingMethod;
    private String PaymentMethod;
    private boolean SelectedPickUpInStore=false;

    public com.bs.ecommerce.model.BillingAddress getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(com.bs.ecommerce.model.BillingAddress billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        PaymentMethod = paymentMethod;
    }

    public com.bs.ecommerce.model.BillingAddress getShippingAddress() {
        return ShippingAddress;
    }

    public void setShippingAddress(com.bs.ecommerce.model.BillingAddress shippingAddress) {
        ShippingAddress = shippingAddress;
    }

    public String getShippingMethod() {
        return ShippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        ShippingMethod = shippingMethod;
    }


    public com.bs.ecommerce.model.BillingAddress getPickupAddress() {
        return PickupAddress;
    }

    public void setPickupAddress(com.bs.ecommerce.model.BillingAddress pickupAddress) {
        PickupAddress = pickupAddress;
    }

    public boolean isSelectedPickUpInStore() {
        return SelectedPickUpInStore;
    }

    public void setSelectedPickUpInStore(boolean selectedPickUpInStore) {
        SelectedPickUpInStore = selectedPickUpInStore;
    }
}
