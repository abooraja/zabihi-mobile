package com.bs.ecommerce.model;

import com.bs.ecommerce.networking.BaseResponse;

/**
 * Created by bs-110 on 12/22/2015.
 */
public class ConfirmPayPalCheckoutResponse extends BaseResponse{
    long OrderId;

    public long getOrderId() {
        return OrderId;
    }

    public void setOrderId(long orderId) {
        OrderId = orderId;
    }
}
