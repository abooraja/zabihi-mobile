package com.bs.ecommerce.model;

/**
 *  on 11/9/2015.
 */
public class ProductPrice {
    private String OldPrice;
    private String Price;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getOldPrice() {
        return OldPrice;
    }

    public void setOldPrice(String oldPrice) {
        OldPrice = oldPrice;
    }
}
