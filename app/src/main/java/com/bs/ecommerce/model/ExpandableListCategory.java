package com.bs.ecommerce.model;

import java.util.List;

/**
 * .
 */
public class ExpandableListCategory {
    List<Category> childCategory;
    private Category parentCategory;

    public List<Category> getChildCategory() {
        return childCategory;
    }

    public void setChildCategory(List<Category> childCategory) {
        this.childCategory = childCategory;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }
}
