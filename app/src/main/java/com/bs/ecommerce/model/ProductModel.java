package com.bs.ecommerce.model;

/**
 *  on 11/9/2015.
 */
public class ProductModel  extends BaseProductModel{
    private String ShortDescription;
    private com.bs.ecommerce.model.ProductPrice ProductPrice;
    private ReviewModel ReviewOverviewModel;

    public String getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public com.bs.ecommerce.model.ProductPrice getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(com.bs.ecommerce.model.ProductPrice productPrice) {
        ProductPrice = productPrice;
    }

    public ReviewModel getReviewOverviewModel() {
        return ReviewOverviewModel;
    }

    public void setReviewOverviewModel(ReviewModel reviewOverviewModel) {
        ReviewOverviewModel = reviewOverviewModel;
    }
}
