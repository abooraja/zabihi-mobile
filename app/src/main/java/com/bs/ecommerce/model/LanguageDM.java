package com.bs.ecommerce.model;

/**
 * Created by BS-182 on 8/9/2017.
 */

public class LanguageDM {
    private int CurrentLanguageId;

    public int getCurrentLanguageId() {
        return CurrentLanguageId;
    }

    public void setCurrentLanguageId(int currentLanguageId) {
        CurrentLanguageId = currentLanguageId;
    }
}
