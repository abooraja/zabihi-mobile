package com.bs.ecommerce.model;

import com.bs.ecommerce.networking.BaseResponse;

/**
 * .
 */
public class AppThemeResponse extends BaseResponse {
    private AppTheme Data;

    public AppTheme getData() {
        return Data;
    }

    public void setData(AppTheme data) {
        Data = data;
    }

}
