package com.bs.ecommerce.model;

/**
 * .
 */
public class KeyValuePair {
    private String Value;
    private String Key;

    public KeyValuePair(String key, String value) {
        this.Value = value;
        this.Key = key;
    }

    public KeyValuePair() {

    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
