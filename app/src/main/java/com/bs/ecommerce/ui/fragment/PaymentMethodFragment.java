package com.bs.ecommerce.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.PaymentMethod;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.postrequest.ValuePost;
import com.bs.ecommerce.networking.response.PaymentMethodRetrievalResponse;
import com.bs.ecommerce.networking.response.PaymentMethodSaveResponse;
import com.bs.ecommerce.ui.adapter.FragmentClass;
import com.bs.ecommerce.ui.customview.RadioGridGroupforReyMaterial;
import com.bs.ecommerce.ui.views.MethodSelctionProcess;
import com.google.gson.Gson;
import com.rey.material.widget.RadioButton;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.Nullable;
import butterknife.BindView;

/**
 *  on 12/7/2015.
 */
public class PaymentMethodFragment extends BaseFragment implements View.OnClickListener {
    int id=0;
    @BindView(R.id.rg_shipiingMethod)
    RadioGridGroupforReyMaterial radioGridGroup;
    @BindView(R.id.btn_continue)
    Button continueBtn;
    MethodSelctionProcess methodSelctionProcess;
    String PaymentMethodValue="";
    //String PaymentMethodValuePrefix="";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shipping_method,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id=0;
        callApiOfGettingPaymentMethod();
        continueBtn.setOnClickListener(this);
    }

    private void callApiOfGettingPaymentMethod() {
        RetroClient.getApi().getPaymentMethod().enqueue(new CustomCB<PaymentMethodRetrievalResponse>(this.getView()));
    }

    public void onEvent(PaymentMethodRetrievalResponse paymentMethodRetrievalResponse)
    {
        Log.d("paymentMethods",new Gson().toJson(paymentMethodRetrievalResponse));
       addMethodRadioGroup(paymentMethodRetrievalResponse.getPaymentMethods());
    }
    private void addMethodRadioGroup(List<PaymentMethod> paymentMethods) {
        methodSelctionProcess=new MethodSelctionProcess(radioGridGroup);
        for (PaymentMethod method : paymentMethods)
            generateRadioButton(method);
    }


    private void generateRadioButton(final PaymentMethod method) {
        LinearLayout linearLayout = (LinearLayout) getActivity().getLayoutInflater().
                inflate(R.layout.item_payment_method, radioGridGroup, false);

        final RadioButton radioButton = (RadioButton) linearLayout.findViewById(R.id.rb_paymentChoice);
        radioButton.setText(method.getName());
        radioButton.setId(++id);

        if (isPreselected(method)) {
            radioButton.setChecked(true);
            PaymentMethodValue=method.getPaymentMethodSystemName();
        }

        ImageView imageView=(ImageView)linearLayout.findViewById(R.id.iv_paymentMethodImage);
        Picasso.with(getActivity()).load(method.getLogoUrl()).into(imageView);

        radioGridGroup.addView(linearLayout);

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    methodSelctionProcess.resetRadioButton(buttonView.getId());
                    PaymentMethodValue = method.getPaymentMethodSystemName();
                }
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioButton.setChecked(true);
            }
        });

    }

    private boolean isPreselected(PaymentMethod method) {
        return method.isSelected();
    }

    @Override
    public void onClick(View v) {

        if(!PaymentMethodValue.isEmpty())
        RetroClient.getApi().saveCheckoutPaymentMethod(new ValuePost(PaymentMethodValue))
                .enqueue(new CustomCB<PaymentMethodSaveResponse>(this.getView()));
    }

    public void onEvent(PaymentMethodSaveResponse saveResponse)
    {
        if(saveResponse.getStatusCode()==200)
            ((CheckoutStepFragment)getParentFragment()).replaceFragment(FragmentClass.PaymentMethod);
    }

}

