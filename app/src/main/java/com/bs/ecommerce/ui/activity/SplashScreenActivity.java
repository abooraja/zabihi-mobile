package com.bs.ecommerce.ui.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.bs.ecommerce.MainActivity;
import com.bs.ecommerce.R;
import com.bs.ecommerce.model.AppThemeResponse;
import com.bs.ecommerce.service.PreferenceService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
/**
 * .
 */
public class SplashScreenActivity extends Activity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 5000;
    private static final String TAG = SplashScreenActivity.class.getCanonicalName();
    private static String CONTAINER_ID = "GTM-WPHVH7";
    @BindView(R.id.iv_background)
    ImageView backgroundImageview;
    BroadcastReceiver mRegistrationBroadcastReceiver;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        callGCM();
        initializeData();

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

    }


    private void callGCM() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = PreferenceService.getInstance().GetPreferenceBooleanValue(PreferenceService.SENT_TOKEN_TO_SERVER);
            }
        };
    }

    private void initializeData() {
        Picasso.with(this).load(R.drawable.splash).into(backgroundImageview);
        startThread();
    }

    private void startThread() {
        final Handler handler = new Handler();
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        startMainActivity();
                    }
                }, 3000);
            }
        }, 0);
    }


    private void startMainActivity() {
        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(PreferenceService.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    public void onEvent(AppThemeResponse appThemeResponse)
    {

    }


}
