package com.bs.ecommerce.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.bs.ecommerce.R;
import com.bs.ecommerce.event.EditAddressEvent;
import com.bs.ecommerce.event.RemoveAddressEvent;
import com.bs.ecommerce.model.CustomerAddress;
import com.bs.ecommerce.model.CustomerAddressResponse;
import com.bs.ecommerce.model.RemoveCustomerAddressResponse;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.adapter.CustomerAddressAdapter;
import com.bs.ecommerce.utils.Language;
import com.google.gson.Gson;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import de.greenrobot.event.EventBus;

//import javax.inject.Inject;


/**
 * Created by bs-110 on 12/15/2015.
 */
public class CustomerAddressesFragment extends BaseFragment implements View.OnClickListener {


    @BindView(R.id.btn_add)
    Button addAddressButton;

    @BindView(R.id.recycler_view_address)
    RecyclerView mRecyclerView;

    private ArrayList<CustomerAddress> customerAddresses;
    private CustomerAddressAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_addresses, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        getActivity().setTitle(getString(R.string.addresses));
        addAddressButton.setOnClickListener(this);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        customerAddresses = new ArrayList<>();
        mAdapter = new CustomerAddressAdapter(getActivity(),customerAddresses,preferenceService);
        mRecyclerView.setAdapter(mAdapter);

        callGetAddressesWebservice();
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.PERSIAN)){
            mRecyclerView.setRotationY(180);
        }
    }

    public void callGetAddressesWebservice() {
        RetroClient.getApi().getCustomerAddresses().enqueue(new CustomCB<CustomerAddressResponse>(this.getView()));
    }

    public void callRemoveAddressWebservice(int addressId) {
        RetroClient.getApi().removeCustomerAddresses(addressId)
                .enqueue(new CustomCB<RemoveCustomerAddressResponse>(this.getView()));
    }

    public void onEvent(CustomerAddressResponse response){
        if(response.getStatusCode() == 200) {
            //Log.d("ADDRESSES", String.valueOf(response.getExistingAddresses()));
            customerAddresses.clear();
            customerAddresses.addAll(response.getExistingAddresses());
            mAdapter.notifyDataSetChanged();

            if(customerAddresses.size() == 0){
                showSnack(getString(R.string.no_address_found));
            }
        }
    }

    public void onEvent(RemoveCustomerAddressResponse response){
        String message = "";
        if(response.getStatusCode() == 200) {
            message = getString(R.string.address_remove_succssfully);
        } else {
            message = getString(R.string.error_removing_address);
        }

        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void onEvent(EditAddressEvent event){

        Fragment editCustomerFragment= new CustomerAddAddressFragment();
        Bundle args = new Bundle();
        args.putInt("index", event.getIndex()+1);
        args.putString("addressJson", new Gson().toJson(event.getAddress()));
        editCustomerFragment.setArguments(args);

        getFragmentManager().beginTransaction().replace(R.id.container,
                editCustomerFragment)
                .addToBackStack(null).commit();
    }

    public void onEvent(RemoveAddressEvent event){
        showRemoveAddressConfirmationDialog(event.getAddress());
    }

    private void showRemoveAddressConfirmationDialog(final CustomerAddress address){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.are_you_sure_delete_address)
                .setTitle(R.string.delete_address);

        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mAdapter.remove(address);
                callRemoveAddressWebservice(Integer.parseInt(address.getId()));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_add){
            getFragmentManager().beginTransaction().replace(R.id.container,
                    new CustomerAddAddressFragment())
                    .addToBackStack(null).commit();
        }
    }
}
