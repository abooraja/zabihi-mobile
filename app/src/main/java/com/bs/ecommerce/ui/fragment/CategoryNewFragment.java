package com.bs.ecommerce.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bs.ecommerce.Constants;
import com.bs.ecommerce.R;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.response.CategoryNewResponse;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.activity.BaseActivity;
import com.bs.ecommerce.ui.adapter.CategoryListAdapter;
import com.bs.ecommerce.utils.Language;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * .
 */
public class CategoryNewFragment extends BaseFragment {
    @BindView(R.id.ll_rootLayout)
    RelativeLayout RootViewLinearLayout;
    @BindView(R.id.expandList)
    RecyclerView expandableRecylerview;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.category, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utility.setActivity(getActivity());
        getBaseUrl();

        preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, Language.PERSIAN);
    }

    @Override
    public void onStart() {
        super.onStart();
        RetroClient.getApi().getNewCategory().enqueue(new CustomCB<CategoryNewResponse>(RootViewLinearLayout));

    }

    public void getBaseUrl() {
        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.DO_USE_NEW_URL)) {
            Constants.BASE_URL = preferenceService
                    .GetPreferenceValue(PreferenceService.URL_PREFER_KEY);
        }
    }

    public void onEvent(CategoryNewResponse response) {
        if (response.getCount() > 0) {
            Utility.setCartCounter(response.getCount());
        }
        expandableRecylerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        expandableRecylerview.setAdapter(new CategoryListAdapter(getActivity(), response.getData(), preferenceService, this));

        String languageToLoad;
        if (response.getLanguage()!=null){
            if (response.getLanguage().getCurrentLanguageId() == 1) {
                languageToLoad = Language.ENGLISH;
            } else {
                languageToLoad = Language.PERSIAN;
            }
            //MJVAKILI
            languageToLoad = Language.PERSIAN;
            preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, languageToLoad);
            ((BaseActivity)getActivity()).setLocale(true);
        }
        preferenceService.SetPreferenceValue(PreferenceService.taxShow, response.isDisplayTaxInOrderSummary());
        preferenceService.SetPreferenceValue(PreferenceService.discuntShow, response.isShowDiscountBox());

    }



}
