package com.bs.ecommerce.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.bs.ecommerce.Constants;
import com.bs.ecommerce.MainActivity;
import com.bs.ecommerce.R;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.NetworkUtil;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.response.CategoryResponse;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.views.FormViews;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * .
 */
//@ContentView(R.layout.activity_base_url)
public class BaseUrlChangeActivity extends BaseActivity implements View.OnClickListener {

    public DrawerLayout drawerLayout;
    @BindView(R.id.btn_test_url)
    Button testUrlBtn;
    @BindView(R.id.btn_main_url)
    Button mainUrlBtn;
    @BindView(R.id.et_new_base_url)
     EditText newBaseUrlEditText;
    @BindView(R.id.app_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_url);
        ButterKnife.bind(this);
        buttonClickListener();
        textChangeListener();
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.change_base_url);
        }
    }



    private void buttonClickListener() {
        testUrlBtn.setOnClickListener(this);
        mainUrlBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int resourceId = v.getId();

        if(resourceId == testUrlBtn.getId()) {
            validateForm();
        } else if(resourceId == mainUrlBtn.getId()) {
             keepOldUrl();
        }
    }

    private void keepOldUrl() {
        Constants.BASE_URL = Constants.DEFAULT_URL;
        preferenceService.SetPreferenceValue(PreferenceService.DO_USE_NEW_URL, false);
        forceRunApp();
    }

    private void validateForm() {
        if (FormViews.getTexBoxFieldValue(newBaseUrlEditText).length() > 7) {
            TestApiCall();
        } else {
            newBaseUrlEditText.setError(getString(R.string.url_is_require));
        }
    }

    private void TestApiCall() {
        String url = FormViews.getTexBoxFieldValue(newBaseUrlEditText) + "/api/";

         RelativeLayout viewGroup = (RelativeLayout)
                 ((ViewGroup)getActivityContentView()).getChildAt(0);
        RetroClient.getApi().getCategory().enqueue(new CustomCB<CategoryResponse>(viewGroup));
    }

    private void changeBaseUrl() {
        String url = FormViews.getTexBoxFieldValue(newBaseUrlEditText)+"/api/";
        Constants.BASE_URL = url;
        preferenceService.SetPreferenceValue(PreferenceService.URL_PREFER_KEY, url);
        preferenceService.SetPreferenceValue(PreferenceService.DO_USE_NEW_URL, true);

    }

    public  void forceRunApp( ){
        NetworkUtil.setToken("");
        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, false);
        EventBus.getDefault().unregister(this);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }


    public void onEvent(CategoryResponse categoryResponse) {
        changeBaseUrl();
        forceRunApp();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void textChangeListener()
    {
        newBaseUrlEditText.setText("http://");
        Selection.setSelection(newBaseUrlEditText.getText(), newBaseUrlEditText.getText().length());


        newBaseUrlEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("http://")){
                    newBaseUrlEditText.setText("http://");
                    Selection.setSelection(newBaseUrlEditText.getText(), newBaseUrlEditText.getText().length());

                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}
