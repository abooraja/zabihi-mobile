package com.bs.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.BaseProductModel;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * .
 */
public class RelatedProductAdapter extends ProductAdapter {
    public RelatedProductAdapter(Context context, List<? extends BaseProductModel> productsList) {
        super(context, productsList);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products_grid_related,parent, false);

        return new ProductSummaryHolder(itemView);
    }
}
