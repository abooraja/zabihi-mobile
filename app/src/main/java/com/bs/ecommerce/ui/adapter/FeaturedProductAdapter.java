package com.bs.ecommerce.ui.adapter;

/**
 *
 */
import android.content.Context;

import com.bs.ecommerce.model.BaseProductModel;
import com.bs.ecommerce.model.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FeaturedProductAdapter extends HomePageProductAdapter {
    public FeaturedProductAdapter(Context context, List<? extends BaseProductModel> productsList) {
        super(context, productsList);
    }

    @Override
    public void onBindViewHolder(ProductSummaryHolder holder, int position) {
        ProductModel product=(ProductModel)productsList.get(position);
        Picasso.with(context).load(product.getDefaultPictureModel().getImageUrl())
                .fit().centerCrop().into(holder.productImage);
        holder.productName.setText(product.getName());
        holder.productOldPrice.setText(product.getProductPrice().getOldPrice());
        holder.productPrice.setText(product.getProductPrice().getPrice());
    }
}
