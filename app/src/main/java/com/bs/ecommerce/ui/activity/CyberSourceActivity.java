package com.bs.ecommerce.ui.activity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bs.ecommerce.Constants;
import com.bs.ecommerce.R;
import com.bs.ecommerce.networking.NetworkUtil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * .
 */
//@ContentView(R.layout.activity_cyber_source)
public class CyberSourceActivity extends AppCompatActivity {

    @BindView(R.id.wv_cyber_source)
    WebView cyberSourceWebView;
    private String url = Constants.BASE_URL + "/checkout/OpcCompleteRedirectionPayment";
    private String toolbarTitle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cyber_source);
        ButterKnife.bind(this);
        setWebViewRequiredInfo();
        toolbarTitle = getString(R.string.cyber_source_payment);
        setToolbar();
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(toolbarTitle);
        }

    }

    private void setWebViewRequiredInfo() {
        cyberSourceWebView.getSettings().setJavaScriptEnabled(true);
        cyberSourceWebView.getSettings().setDomStorageEnabled(true);
        cyberSourceWebView.getSettings().setBuiltInZoomControls(true);
        cyberSourceWebView.getSettings().setDisplayZoomControls(false);
        cyberSourceWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url, NetworkUtil.getHeaders());
                return false;
            }
        });
        cyberSourceWebView.loadUrl(url, NetworkUtil.getHeaders());
    }

}
