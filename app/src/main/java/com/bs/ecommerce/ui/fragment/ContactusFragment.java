package com.bs.ecommerce.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.bs.ecommerce.R;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.views.FormViews;
import com.bs.ecommerce.utils.Language;

import androidx.annotation.Nullable;
import butterknife.BindView;

/**
 * .
 */
public class ContactusFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.et_name)
     EditText nameEditText;

    @BindView(R.id.et_email)
    EditText emailEditText;

    @BindView(R.id.et_enquiry)
    EditText enquiryEditText;

    @BindView(R.id.btn_email)
    Button emailBtn;

    @BindView(R.id.btn_sms)
    Button smsBtn;
    @BindView(R.id.btn_call)
    Button callBtn;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.fragment_contact_us,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getString(R.string.contact_us));
        smsBtn.setOnClickListener(this);
        emailBtn.setOnClickListener(this);
        callBtn.setOnClickListener(this);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.PERSIAN)){
            enquiryEditText.setGravity(Gravity.RIGHT);
        }

    }

    @Override
    public void onClick(View v) {
        int resourceId=v.getId();
        if(resourceId== R.id.btn_email) {
            if (validateForm())
                sendEmail();
        }
            else if(resourceId== R.id.btn_sms)

            {
                if(validateForm())
                    sendSms();
            }
            else if(resourceId== R.id.btn_call)
            {
                callUs();
            }


    }



    private boolean validateForm()
    {
      boolean isValid=true;
       /* if(!FormViews.isValidWithMark(nameEditText,"Name"))
            isValid=false;*/
       /* if(!FormViews.isValidWithMark(emailEditText,"Email"))
            isValid=false;*/
        if(!FormViews.isValidWithMark(enquiryEditText,"Enquiry"))
            isValid=false;
        /*if(isValid)
            sendEmail();*/
        return isValid;
    }

    private void sendEmail()
    {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "admin@zabihikala.ir"});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.contact));
        intent.putExtra(Intent.EXTRA_TEXT, FormViews.getTexBoxFieldValue(enquiryEditText));

        startActivity(Intent.createChooser(intent, getString(R.string.send_emil)));
    }

    private void sendSms()
    {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address","+989137779506");
        smsIntent.putExtra("sms_body", FormViews.getTexBoxFieldValue(enquiryEditText));
        startActivity(smsIntent);
    }

    private void callUs() {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:+989137779506"));
        startActivity(callIntent);
    }
}
