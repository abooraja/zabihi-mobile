package com.bs.ecommerce.ui.fragment;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.CustomerRegistrationInfo;
import com.bs.ecommerce.model.LoginData;
import com.bs.ecommerce.model.LoginResponse;
import com.bs.ecommerce.model.RegistrationResponse;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.NetworkUtil;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.utils.UiUtils;

import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;

/**
 * Created by bs-110 .
 */
public class RegisterFragment extends BaseFragment {

    @BindView(R.id.tvName)
    TextView nameTitleTextView;

    @BindView(R.id.customer_name)
    TextView customerNameTextView;

    @BindView(R.id.dateOfBirth)
    TextView dateOfBirthTextView;

    @BindView(R.id.customer_email)
    TextView emailTextView;

    @BindView(R.id.company_info)
    TextView companyInfoTextView;

    @BindView(R.id.gender)
    TextView genderTextView;

    @BindView(R.id.et_customer_first_name)
    EditText customerFirstNameEditText;

    @BindView(R.id.et_customer_last_name)
    EditText customerLastNameEditText;


    @BindView(R.id.et_customer_email)
    EditText emailEditText;

    @BindView(R.id.et_company_info)
    EditText companyInfoEditText;

    @BindView(R.id.rb_male)
    RadioButton genderMaleRadioButton;

    @BindView(R.id.rb_female)
    RadioButton genderFemaleRadioButton;

    @BindView(R.id.genderRadioGroup)
    RadioGroup genderRadioGroup;

    @BindView(R.id.btn_save)
    Button saveBtn;

    @BindView(R.id.cb_newsletter)
    CheckBox cbNewsLetter;

    @BindView(R.id.et_password)
    EditText passwordEditText;

    @BindView(R.id.et_confirm_password)
    EditText confirmPasswordEditText;
    @BindView(R.id.tv_password)
    TextView passwordTextView;

    @BindView(R.id.customer_username)
    TextView usernameTextView;
    @BindView(R.id.et_customer_username)
    EditText usernameEditText;

    @BindView(R.id.customer_phone)
    TextView phoneTextView;
    @BindView(R.id.et_customer_phone)
    EditText phoneEditText;

    PersianCalendar myCalendar = null;
    DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
//            myCalendar = Calendar.getInstance();
//            myCalendar.set(Calendar.YEAR, year);
//            myCalendar.set(Calendar.MONTH, monthOfYear);
//            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//            dateOfBirthTextView.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
//            saveBtn.setVisibility(View.VISIBLE);
        }

    };
    private CustomerRegistrationInfo customerInfo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_basic_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEventBusRegistration();
        getActivity().setTitle(getString(R.string.register));

        clearUI();
        hideTextPanels();
        initEditButtonsAction();

    }

    private void callRegisterWebService() {
        RetroClient.getApi().preformRegistration(customerInfo).enqueue(new CustomCB<RegistrationResponse>(this.getView()));
    }

    public void callLoginWebservice(LoginData loginData) {
        RetroClient.getApi().performLogin(loginData).enqueue(new CustomCB<LoginResponse>(this.getView()));
    }

    public void onEvent(LoginResponse response) {
        if (response.getToken() != null) {
            preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, response.getToken());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
            /*NetworkUtilities.token=response.getToken();*/
            NetworkUtil.setToken(response.getToken());
            getFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);//.popBackStack();
//            getFragmentManager().beginTransaction().replace(R.id.container,
//                    new MyAccountFragment()).commit();
        }
    }

    public void onEvent(RegistrationResponse response) {
        if (response.getStatusCode() == 400) {
            saveBtn.setEnabled(true);
            String errors = getString(R.string.error_register_customer) + "\n";
            if (response.getErrorList().length > 0) {
                for (int i = 0; i < response.getErrorList().length; i++) {
                    errors += "  " + (i + 1) + ": " + response.getErrorList()[i] + " \n";
                }
                Toast.makeText(getActivity(), errors, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.customer_succes_msg), Toast.LENGTH_LONG).show();
            callLoginWebservice(new LoginData(customerInfo.getEmail(), customerInfo.getPassword()));
        }
    }

    private void hideTextPanels() {
        nameTitleTextView.setVisibility(View.GONE);
        customerNameTextView.setVisibility(View.GONE);
        emailTextView.setVisibility(View.GONE);
        companyInfoTextView.setVisibility(View.GONE);
        genderTextView.setVisibility(View.GONE);
        usernameTextView.setVisibility(View.GONE);
        saveBtn.setText(getString(R.string.register_new));
        dateOfBirthTextView.setText("dd/mm/yyyy");
        passwordTextView.setVisibility(View.GONE);
        phoneTextView.setVisibility(View.GONE);
    }

    private void clearUI() {
        customerNameTextView.setText("");
        dateOfBirthTextView.setText("");
        emailTextView.setText("");
        companyInfoTextView.setText("");
        genderTextView.setText("");
        usernameTextView.setText("");
        phoneTextView.setText("");

        customerFirstNameEditText.setText("");
        customerLastNameEditText.setText("");
        emailEditText.setText("");
        companyInfoEditText.setText("");
        usernameEditText.setText("");
        phoneEditText.setText("");

    }

    private void initEditButtonsAction() {

        dateOfBirthTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //Calendar c = Calendar.getInstance();
                PersianCalendar c = new PersianCalendar();
                if (myCalendar != null) {
                    c = myCalendar;
                }

                PersianDatePickerDialog picker = new PersianDatePickerDialog(getActivity())
                        .setPositiveButtonString("باشه")
                        .setNegativeButton("بیخیال")
                        .setTodayButton("امروز")
                        .setTodayButtonVisible(true)
                        .setInitDate(c)
                        //.setMaxYear(1395)
                        .setMinYear(1300)
                        .setActionTextColor(Color.GRAY)
//                        .setTypeFace(typeface)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar persianCalendar) {
                                // TODO Auto-generated method stub
//                                myCalendar = Calendar.getInstance();
//                                myCalendar.set(Calendar.YEAR, year);
//                                myCalendar.set(Calendar.MONTH, monthOfYear);
//                                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                myCalendar = persianCalendar;
                                dateOfBirthTextView.setText(persianCalendar.getPersianYear() + "/" + (persianCalendar.getPersianMonth()) + "/" + persianCalendar.getPersianDay());
                                saveBtn.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
//                new DatePickerDialog(getActivity(), dateSetListener, c
//                        .get(Calendar.YEAR), c.get(Calendar.MONTH),
//                        c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performRegistration();
                saveBtn.setEnabled(false);
                UiUtils.hideSoftKeyboard(getActivity());
            }
        });


    }

    private void performRegistration() {
        getCustomerInfo();
        if (isValidCustomerInfo()) {
            callRegisterWebService();
        }
    }

    private boolean isValidCustomerInfo() {
        return true;
    }

    private void getCustomerInfo() {
        customerInfo = new CustomerRegistrationInfo();
        customerInfo.setFirstName(customerFirstNameEditText.getText().toString());
        customerInfo.setLastName(customerLastNameEditText.getText().toString());
        customerInfo.setEmail(emailEditText.getText().toString().trim());
        customerInfo.setPhone(phoneEditText.getText().toString());
        customerInfo.setCompany(companyInfoEditText.getText().toString());
        if (myCalendar != null) {
            customerInfo.setDateOfBirthYear(myCalendar.get(Calendar.YEAR));
            customerInfo.setDateOfBirthMonth(myCalendar.get(Calendar.MONTH) + 1);
            customerInfo.setDateOfBirthDay(myCalendar.get(Calendar.DAY_OF_MONTH));
        }
        if (genderMaleRadioButton.isChecked()) {
            customerInfo.setGender("M");
        } else if (genderFemaleRadioButton.isChecked()) {
            customerInfo.setGender("F");
        }
        customerInfo.setNewsletter(cbNewsLetter.isChecked());
        customerInfo.setPassword(passwordEditText.getText().toString());
        customerInfo.setConfirmPassword(confirmPasswordEditText.getText().toString());
        customerInfo.setUsername(usernameEditText.getText().toString().trim());
    }

}
