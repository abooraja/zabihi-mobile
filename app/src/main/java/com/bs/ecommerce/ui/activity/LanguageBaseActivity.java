package com.bs.ecommerce.ui.activity;

import android.content.Context;
import android.os.Bundle;

import com.bs.ecommerce.utils.ContextWrapper;
import com.bs.ecommerce.utils.Language;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;


public class LanguageBaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        // .. create or get your new Locale object here.
//        preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, Language.ITALIAN);
        Locale newLocale = new Locale(Language.ENGLISH);

        Context context = ContextWrapper.wrap(newBase, newLocale);
        super.attachBaseContext(context);
    }

}
