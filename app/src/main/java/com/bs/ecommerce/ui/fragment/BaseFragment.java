package com.bs.ecommerce.ui.fragment;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bs.ecommerce.MainActivity;
import com.bs.ecommerce.R;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.activity.BaseActivity;
import com.google.android.material.snackbar.Snackbar;
import com.pnikosis.materialishprogress.ProgressWheel;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.NoSubscriberEvent;

/**
 *  on 11/5/2015.
 */
public class BaseFragment extends Fragment {
   /* @javax.annotation.Nullable
    @BindView(R.id.app_toolbar)
   */

    public PreferenceService preferenceService;
    Toast mToast;
    ProgressWheel progress;
    private Toolbar toolbar2;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceService = PreferenceService.getInstance();

        ((BaseActivity)getActivity()).setLocale(false);
        EventBus.getDefault().register(this);
        setHasOptionsMenu(true);
    }

    //    @Override
//    public void onAttach(Context context) {
//        AndroidSupportInjection.inject(this);
//        super.onAttach(context);
//    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((BaseActivity) getActivity()).setLocale(false);
        //EventBus.getDefault().register(this);
        checkEventBusRegistration();
        setHasOptionsMenu(true);

        View view = null;
        if (getLayoutId() != 0) {
            view = inflater.inflate(getLayoutId(), container, false);
            unbinder = ButterKnife.bind(this, view);

        }

//        RoboGuice.getInjector(getActivity()).injectMembersWithoutViews(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

     @Override
     public void onStart() {
         super.onStart();

     }
    @Override
    public void onResume() {
      checkEventBusRegistration();

        super.onResume();
      //  setToolbar();

    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    protected int getLayoutId() {
        return 0;
    }

    //    protected  LayoutInflater getActivity().getLayoutInflater()
//    {
//        LayoutInflater inflater = (LayoutInflater) getActivity()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        return inflater;
//    }
    public void checkEventBusRegistration()
    {
        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this);
        }
    }

    public void onEvent( NoSubscriberEvent noSubscriberEvent)
    {

    }
    public void onEvent( ClassCastException noSubscriberEvent)
    {

    }
    public void setToolbar()
    {
        try{
            if(toolbar2!=null)
                MainActivity.self.setToolbar(toolbar2);
        }
        catch (Exception Ex)
        {

        }
    }

    public void goMenuItemFragment(Fragment fragment) {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    protected void gotoNewFragment(Fragment fragment)
    {
        getFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

    }

    public void showSnack(String message){
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
    }


    public void showToast(String msg){
        if(mToast != null){
            mToast.cancel();
        }
        mToast = Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG);
        mToast.show();
    }

    protected void showProgress(RelativeLayout layout) {
        RelativeLayout.LayoutParams params = new RelativeLayout
                .LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        progress = (ProgressWheel) getActivity().getLayoutInflater().inflate(R.layout.materialish_progressbar, null);
        layout.addView(progress, params);
        progress.spin();
    }

    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
