package com.bs.ecommerce.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bs.ecommerce.R;
import com.bs.ecommerce.event.RemoveWishlistItemEvent;
import com.bs.ecommerce.model.CartProduct;
import com.bs.ecommerce.model.KeyValuePair;
import com.bs.ecommerce.model.WishistUpdateResponse;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.response.CartProductListResponse;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.adapter.WishlistAdapter;
import com.bs.ecommerce.ui.customview.CustomLinearLayoutManager;
import com.bs.ecommerce.utils.Language;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 * Created by bs-110 on 12/24/2015.
 */
public class WishlistFragment extends BaseFragment {

    @BindView(R.id.btn_add_all_items_to_cart)
    Button addAllItemsToCartBtn;

    @BindView(R.id.rclv_wish_list)
    RecyclerView wishRecyclerList;

    CustomLinearLayoutManager layoutManager;
    List<CartProduct> wishListProduct;
    WishlistAdapter wishListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wishlist,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle(getString(R.string.wishlist));
        setLayoutManagerofRecyclerList();
        checkEventBusRegistration();
        callWebservice();
        wishRecyclerList.setNestedScrollingEnabled(false);
        if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.PERSIAN)){
            wishRecyclerList.setRotationY(180);
        }
        addAllItemsToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemsToCart();
            }
        });

    }

    public void onEvent(WishistUpdateResponse response){
        if(response.getStatusCode() != 200){
            showSnack(getString(R.string.error_removing_item));
        } else {
            populatedDatainAdapter(response.getItems());
        }
    }

    public void callWebservice() {
        RetroClient.getApi().getWishList().enqueue(new CustomCB<CartProductListResponse>(this.getView()));
    }

    public void onEvent(CartProductListResponse cartProductListResponse)
    {
        if(cartProductListResponse!=null && cartProductListResponse.getItems()!=null) {
            if (cartProductListResponse.getItems().size() > 0) {
                populatedDatainAdapter(cartProductListResponse.getItems());


            } else {
                wishListAdapter=new WishlistAdapter(getActivity(),new ArrayList(),this,preferenceService);
                wishRecyclerList.setAdapter(wishListAdapter);
                addAllItemsToCartBtn.setVisibility(View.GONE);
                Snackbar.make(getView(), R.string.wishlist_empty, Snackbar.LENGTH_SHORT).show();
            }

            if(cartProductListResponse.getCount()>0)
                Utility.setCartCounter(cartProductListResponse.getCount());
        }

    }

    private void addItemsToCart(){
        if(wishListProduct.size() > 0) {
            callWebService();
        }
    }

    private void callWebService(){
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        for (CartProduct cp: wishListProduct) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey("addtocart");
            keyValuePair.setValue(cp.getId()+"");
            keyValuePairs.add(keyValuePair);
        }

        RetroClient.getApi().addAllItemsToCartFromWishList(keyValuePairs)
                .enqueue(new CustomCB<CartProductListResponse>(getView()));
    }

    public void populatedDatainAdapter(List<CartProduct> cartProductList)
    {
        this.wishListProduct = new ArrayList<>();
        this.wishListProduct.addAll(cartProductList);
        wishListAdapter=new WishlistAdapter(getActivity(),this.wishListProduct,this,preferenceService);
        wishRecyclerList.setAdapter(wishListAdapter);

    }

    private void setLayoutManagerofRecyclerList()
    {
        layoutManager=new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL,false);
        wishRecyclerList.setHasFixedSize(true);
        wishRecyclerList.setLayoutManager(layoutManager);
    }

    public void onEvent(RemoveWishlistItemEvent event){
        if(event.getCount() == 0) {
            addAllItemsToCartBtn.setVisibility(View.GONE);
            showSnack(getString(R.string.wishlist_empty));
        }
    }

  /*

    public void onEvent(AddItemsToCartFromWishlistResponse response)
    {
        if(response.getStatusCode() == 200){
            Utility.setCartCounter(response.getCount());
          //  populatedDatainAdapter(new ArrayList<CartProduct>());
           // addAllItemsToCartBtn.setVisibility(View.GONE);
            showSnack("Item added to cart");
        } else {
            showSnack("Something went wrong. Please try again later");
        }

    }

    public  void onEvent(AddAllItemsToCartFromWishlistResponse response)
    {
        if(response.getStatusCode() == 200){

            Utility.setCartCounter(response.getCount());
            showSnack("Items added to cart");
            addAllItemsToCartBtn.setVisibility(View.GONE);
            wishListAdapter=new WishlistAdapter(getActivity(),new ArrayList(),this);
             wishRecyclerList.setAdapter(wishListAdapter);
            Snackbar.make(getView(), "Wishlist is Empty", Snackbar.LENGTH_SHORT).show();


        } else {
            showSnack("Something went wrong. Please try again later");
        }
    }
   @BindView(R.id.table_orderTotal)
    TableLayout orderSummaryRelativeLayout;

    @BindView(R.id.cv_orderTotal)
    CardView orderTotalCardView;

    @BindView(R.id.ll_order_totla)
    LinearLayout orderToalLinearLayout;

    @BindView(R.id.ll_cartInfoLayout)
    LinearLayout CartInfoLinearLayout;

    @BindView(R.id.coupon_layout)
    RelativeLayout couponLayout;

    @BindView(R.id.btn_proceed_to_Checkout)
    Button checkoutBtn;

    @BindView(R.id.rclv_cartList)
    RecyclerView cartproductRecyclerList;

    @BindView(R.id.cv_product_attribute)
    CardView productAttributeCardView;

    CustomLinearLayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("Wishlist");
        setLayoutManagerofRecyclerList();

        checkEventBusRegistration();
        callWebservice();

        orderTotalCardView.setVisibility(View.GONE);
        couponLayout.setVisibility(View.GONE);
        checkoutBtn.setText("ADD TO CART");

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemsToCart();
            }
        });

    }

    public void onEvent(WishistUpdateResponse response){
        if(response.getStatusCode() != 200){
            showSnack("Error removing item. Check your internet connection.");
        } else {

        }
    }

    public void callWebservice() {
        RestClient.get().getWishlist(new CustomCallback<CartProductListResponse>(this.getView()));
    }

    public void onEvent(CartProductListResponse cartProductListResponse)
    {
        if(cartProductListResponse!=null && cartProductListResponse.getItems()!=null) {
            if (cartProductListResponse.getItems().size() > 0) {
                populatedDatainAdapter(cartProductListResponse.getItems());
                CartInfoLinearLayout.setVisibility(View.VISIBLE);
                couponLayout.setVisibility(View.GONE);
                orderTotalCardView.setVisibility(View.GONE);
                productAttributeCardView.setVisibility(View.GONE);
            } else {
                Utility.setCartCounter(0);
                Snackbar.make(getView(), "Wishlist is Empty", Snackbar.LENGTH_SHORT).show();
            }
        }

    }

    private void addItemsToCart(){
        if(wishListProduct.size() > 0) {
            callWebService();
        }
    }

    private void callWebService(){
        List<KeyValuePair> keyValuePairs = new ArrayList<>();
        for (CartProduct cp: wishListProduct) {
            KeyValuePair keyValuePair = new KeyValuePair();
            keyValuePair.setKey("addtocart");
            keyValuePair.setValue(cp.getId()+"");
            keyValuePairs.add(keyValuePair);
        }
        RestClient.get().addItemsToCartFromWishlist(keyValuePairs, new CustomCallback<AddItemsToCartFromWishlistResponse>(getView()));
    }

    public void onEvent(AddItemsToCartFromWishlistResponse response)
    {
        if(response.getStatusCode() == 200){
            Utility.setCartCounter(response.getCount());
            populatedDatainAdapter(new ArrayList<CartProduct>());
            checkoutBtn.setVisibility(View.GONE);
            showSnack("Items added to cart");
        } else {
            showSnack("Something went wrong. Please try again later");
        }

    }

    List<CartProduct> wishListProduct;
    WishlistAdapter wishListAdapter;
    public void populatedDatainAdapter(List<CartProduct> wishListProduct)
    {
        this.wishListProduct = new ArrayList<>();
        this.wishListProduct.addAll(wishListProduct);
        wishListAdapter=new WishlistAdapter(getActivity(),this.wishListProduct,this);
        cartproductRecyclerList.setAdapter(wishListAdapter);

    }

    private void setLayoutManagerofRecyclerList()
    {
        layoutManager=new CustomLinearLayoutManager(getActivity(), CustomLinearLayoutManager.VERTICAL,false);
        cartproductRecyclerList.setHasFixedSize(true);
        cartproductRecyclerList.setLayoutManager(layoutManager);
    }

    public void onEvent(RemoveWishlistItemEvent event){
        if(event.getCount() == 0) {
            checkoutBtn.setVisibility(View.GONE);
            showSnack("Wishlist is empty");
        }
    }
*/}
