package com.bs.ecommerce.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.LoginData;
import com.bs.ecommerce.model.LoginResponse;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.NetworkUtil;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.utils.UiUtils;

import androidx.annotation.Nullable;
import butterknife.BindView;
import de.greenrobot.event.EventBus;

//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.FacebookSdk;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.facebook.login.widget.LoginButton;

//import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * .
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.login_button)
    Button loginButton;
    @BindView(R.id.register_button)
    Button registerButton;
    @BindView(R.id.et_login_email)
    EditText emailEditText;
    @BindView(R.id.et_login_password)
    EditText passwordEditText;
    @BindView(R.id.forgetPassword)
    TextView forgetPassword;
    //    private CallbackManager callbackManager;
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_login, container, false);
        } else {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
            }
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        getActivity().setTitle(getString(R.string.login));
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);
        forgetPassword.setOnClickListener(this);
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();
//        LoginButton facebook = (LoginButton) rootView.findViewById(R.id.fb);
//        facebook.setReadPermissions("email");
//        facebook.setFragment(this);
//        facebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d("FacebookLogin", "From LoginButton");
//                onFacebookLogin(loginResult);
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//
//            }
//        });
    }


    public void callLoginWebservice(LoginData loginData) {
        RetroClient.getApi().performLogin(loginData).enqueue(new CustomCB<LoginResponse>(this.getView()));
    }


    public void onEvent(LoginResponse response) {
        if (response.getToken() != null) {
            preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, response.getToken());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, true);
            /*NetworkUtilities.token=response.getToken();*/
            NetworkUtil.setToken(response.getToken());
            Log.e("token", ">>" + NetworkUtil.getToken());
            showToast(getString(R.string.login_succssful));
            getFragmentManager().popBackStack();
        } else {
            showSnack(getString(R.string.username_password_not_match));
        }
    }

    @Override
    public void onClick(View v) {
        int resourceId = v.getId();
        if (resourceId == R.id.login_button) {
            performLogin();
            UiUtils.hideSoftKeyboard(getActivity());
        } else if (resourceId == R.id.register_button) {
            getFragmentManager().beginTransaction().replace(R.id.container,
                    new RegisterFragment()).addToBackStack(null).commit();
        } else if (resourceId == forgetPassword.getId()) {
            getFragmentManager().beginTransaction().replace(R.id.container,
                    new ForgotPasswordFragment()).addToBackStack(null).commit();
        }

    }

    private void performLogin() {
        String email = emailEditText.getText().toString().trim();
        String pass = passwordEditText.getText().toString();
        if (email.length() > 0 && pass.length() > 0) {
            callLoginWebservice(new LoginData(email, pass));
        } else {
            showSnack(getString(R.string.username_password_require));
        }
    }

//    private void onFacebookLogin(LoginResult loginResult) {
//        final AccessToken accessToken = loginResult.getAccessToken();
//        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(),
//                new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse response) {
//                        try {
//                            String name = object.getString("name");
//                            String email = object.getString("email");
//
//                            FacebookLogin facebookLogin = new FacebookLogin();
//                            facebookLogin.setUserId(accessToken.getUserId());
//                            facebookLogin.setAccessToken(accessToken.getToken());
//                            facebookLogin.setEmail(email);
//                            facebookLogin.setName(name);
//
//                            RetroClient.getApi().loginUsingFaceBook(facebookLogin).enqueue(new CustomCB<LoginResponse>(getView()));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            LoginManager.getInstance().logOut();
//                            Toast.makeText(getApplicationContext(), getString(R.string.email_permission_require), Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,name,email");
//        graphRequest.setParameters(parameters);
//        graphRequest.executeAsync();
//
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}