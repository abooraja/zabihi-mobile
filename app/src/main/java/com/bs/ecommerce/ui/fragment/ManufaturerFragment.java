package com.bs.ecommerce.ui.fragment;

import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.response.ProductsResponse;

/**
 * .
 */
public class ManufaturerFragment extends ProductListFragment {
    @Override
    public  void callWebService() {
        if (productAdapter != null) {
            productAdapter.resetList();
        }
        resetList = true;
        pageNumber = 1;

        getQueryMap();
        RetroClient.getApi().getProductListByManufacturer(categoryId, queryMapping)
                .enqueue(new CustomCB<ProductsResponse>(rootViewRelativeLayout));
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
