package com.bs.ecommerce.ui.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.BaseProductModel;
import com.bs.ecommerce.model.FeaturedCategory;
import com.bs.ecommerce.model.ImageModel;
import com.bs.ecommerce.model.ProductModel;
import com.bs.ecommerce.model.ProductService;
import com.bs.ecommerce.model.ViewType;
import com.bs.ecommerce.networking.Api;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.response.FeaturedCategoryResponse;
import com.bs.ecommerce.networking.response.HomePageBannerResponse;
import com.bs.ecommerce.networking.response.HomePageCategoryResponse;
import com.bs.ecommerce.networking.response.HomePageManufacturerResponse;
import com.bs.ecommerce.networking.response.HomePageProductResponse;
import com.bs.ecommerce.networking.response.ProductDetailResponse;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.adapter.FeaturedProductAdapter;
import com.bs.ecommerce.ui.adapter.HomePageProductAdapter;
import com.bs.ecommerce.ui.adapter.ProductAdapter;
import com.bs.ecommerce.utils.Language;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

/**
 *  on 11/9/2015.
 */

public class HomePageFragment extends BaseFragment {
    @BindView(R.id.slider)
    SliderLayout sliderLayout;
    @BindView(R.id.rootView)
    RelativeLayout rlRootView;

    @BindView(R.id.vg_featureCategories)
    LinearLayout featureCategoriesLinearLayout;
    @BindView(R.id.vg_featureManufacturer)
    LinearLayout featureManufacturerLinearLayout;
    @BindView(R.id.vg_featureProduct)
    LinearLayout featureProductLinearLayout;
    @BindView(R.id.featuredCategoryContainerLayout)
    LinearLayout featuredCategoryContainerLayout;

    @BindView(R.id.et_search_item)
    TextView searchItemTextView;

    RecyclerView featureCategoriesRv;
    RecyclerView featureProductRv;
    RecyclerView featureManufacturerRv;

    View view;

    boolean isDataLoaded=false;


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getActivity().getResources().getString(R.string.app_name));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_homepage, container, false);

        } else {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkEventBusRegistration();
        setSizeinList();
        if(!isDataLoaded)
         callWebservice();


        searchItemTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().beginTransaction().replace(R.id.container, new SearchFragment()).
                        addToBackStack(null).commit();

            }
        });
    }



    private void callWebservice() {
        RetroClient.getApi().getHomePageBanner(Api.imageSize).enqueue(new CustomCB<HomePageBannerResponse>(rlRootView));
        // RetroClient.getApi().getHomePageCategories(Api.imageSize).enqueue(new CustomCB<HomePageCategoryResponse>(rlRootView));
        RetroClient.getApi().getHomePageProducts(Api.imageSize).enqueue(new CustomCB<HomePageProductResponse>(rlRootView));
        RetroClient.getApi().getHomePageCategoriesWithProduct().enqueue(new CustomCB<FeaturedCategoryResponse>(rlRootView));
        RetroClient.getApi().getHomePageManufacturer(Api.imageSize).enqueue(new CustomCB<HomePageManufacturerResponse>(rlRootView));
    }


    public void setSizeinList() {
        featureCategoriesRv = (RecyclerView) featureCategoriesLinearLayout.findViewById(R.id.rv_product);
        featureManufacturerRv = (RecyclerView) featureManufacturerLinearLayout.findViewById(R.id.rv_product);
        featureProductRv = (RecyclerView) featureProductLinearLayout.findViewById(R.id.rv_product);


        featureCategoriesRv.setLayoutManager(getLinearLayoutManager());
        featureManufacturerRv.setLayoutManager(getLinearLayoutManager());
        featureProductRv.setLayoutManager(getLinearLayoutManager());

        hideBtns(featureProductLinearLayout);
        hideBtns(featureManufacturerLinearLayout);

        setTitle(getString(R.string.feature_product), featureProductLinearLayout);
        setTitle("Feature Categories", featureCategoriesLinearLayout);
        setTitle(getString(R.string.feature_menufecture), featureManufacturerLinearLayout);

        /*featureProductLinearLayout.setVisibility(View.GONE);
        featureCategoriesLinearLayout.setVisibility(View.GONE);
        featureManufacturerLinearLayout.setVisibility(View.GONE);*/


    }

    public LinearLayoutManager getLinearLayoutManager() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        return layoutManager;
    }

    private void setTitle(String title, View view) {
        ((TextView) view.findViewById(R.id.title)).setText(title.toUpperCase());
    }

    public void onEvent(HomePageBannerResponse bannerResponse) {
        if (bannerResponse.getData() != null)
//            ImageSlider.addSliderItem(bannerResponse, sliderLayout);
            sliderLayout.removeAllSliders();
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        for (ImageModel imageModel : bannerResponse.getData()) {
            DefaultSliderView textSliderView = new DefaultSliderView(Utility.getActivity());
            textSliderView.image(imageModel.getImageUrl())
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            Bundle bundle = new Bundle();
            bundle.putInt("isProduct", imageModel.getIsProduct());
            bundle.putInt("ProdOrCatId", imageModel.getProdOrCatId());
            //add your extra information
            textSliderView.bundle(bundle);
            textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(BaseSliderView slider) {
                    Bundle bundle1 = slider.getBundle();
                    int isProduct = bundle1.getInt("isProduct");
                    int catOrProductId = bundle1.getInt("ProdOrCatId");
                    if (catOrProductId != 0) {
                        if (isProduct != 0) {
                            RetroClient.getApi().getProductDetails(catOrProductId)
                                    .enqueue(new CustomCB<ProductDetailResponse>());
                        } else {
                            getFragmentManager().beginTransaction().replace
                                    (R.id.container, ProductListFragmentFor3_8.newInstance("Category", catOrProductId)).addToBackStack(null).commit();
                        }
                    }
                }
            });
            sliderLayout.addSlider(textSliderView);
            sliderLayout.setCustomIndicator((PagerIndicator) view.findViewById(R.id.custom_indicator));
        }
    }

    public void onEvent(HomePageManufacturerResponse manufacturerResponse) {
        Log.d("homePageManufacture", "yes");
        if (manufacturerResponse.getData() != null && manufacturerResponse.getData().size() >0) {
            featureManufacturerLinearLayout.setVisibility(View.VISIBLE);
            TextView title=(TextView)featureManufacturerLinearLayout.findViewById(R.id.title);
            if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.PERSIAN)){
                title.setGravity(Gravity.RIGHT);
            }
            final HomePageProductAdapter homePageProductAdapter = new HomePageProductAdapter(getActivity(), manufacturerResponse.getData());
            featureManufacturerRv.setAdapter(homePageProductAdapter);
            homePageProductAdapter.SetOnItemClickListener(new HomePageProductAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    ProductService.manufactureId = homePageProductAdapter.productsList.get(position).getId();

                    String name = homePageProductAdapter.productsList.get(position).getName();
                    int id = (int) homePageProductAdapter.productsList.get(position).getId();
                    getFragmentManager().beginTransaction().replace
                            (R.id.container, ManufaturerFragment.newInstance(name, id,true)).addToBackStack(null).commit();
                }
            });

        }
    }



    public void onEvent(HomePageCategoryResponse categoryResponse) {
        Log.d("homePageCategories", "yes");
        if (categoryResponse.getData() != null) {
            featureCategoriesLinearLayout.setVisibility(View.VISIBLE);
            HomePageProductAdapter homePageProductAdapter = new HomePageProductAdapter(getActivity(), categoryResponse.getData());
            featureCategoriesRv.setAdapter(homePageProductAdapter);
            addAdapterOnclickListener(homePageProductAdapter);

        }
    }

    public void onEvent(HomePageProductResponse productResponse) {
        Log.d("homePageprofuct", "yes");
        if (productResponse.getData() != null) {
            featureProductLinearLayout.setVisibility(View.VISIBLE);
            TextView title=(TextView)featureProductLinearLayout.findViewById(R.id.title);
            if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.PERSIAN)){
                title.setGravity(Gravity.RIGHT);
            }
            FeaturedProductAdapter featuredProductAdapter = new FeaturedProductAdapter(getActivity(), productResponse.getData());
            featureProductRv.setAdapter(featuredProductAdapter);
            homepageProductClickListener(featuredProductAdapter);
        }
    }

    private void hideBtns(LinearLayout linearLayout) {
        AppCompatImageButton subCatsBtn = (AppCompatImageButton) linearLayout.findViewById(R.id.btn_menu_sub_cats);
        Button viewAllBtn = (Button) linearLayout.findViewById(R.id.btn_view_all);
        subCatsBtn.setVisibility(View.GONE);
        viewAllBtn.setVisibility(View.GONE);
    }

    public void onEvent(FeaturedCategoryResponse featuredCategoryResponse) {
        populateFeaturedCatoriesIntoRecyclerView(featuredCategoryResponse.getData());
    }

    private void populateFeaturedCatoriesIntoRecyclerView(List<FeaturedCategory> featuredCategories) {
        isDataLoaded=true;
        LayoutInflater inflater = getActivity().getLayoutInflater();
        featuredCategoryContainerLayout.removeAllViews();
        for (final FeaturedCategory featuredCategory : featuredCategories) {
            final LinearLayout linearLayout = (LinearLayout) inflater.inflate
                    (R.layout.merge_product_list_with_header, featureCategoriesLinearLayout, false);
            linearLayout.setVisibility(View.VISIBLE);

            TextView title = (TextView) linearLayout.findViewById(R.id.title);
            final AppCompatImageButton subCatsBtn = (AppCompatImageButton) linearLayout.findViewById(R.id.btn_menu_sub_cats);
            Button viewAllBtn = (Button) linearLayout.findViewById(R.id.btn_view_all);
            RecyclerView productRecyclerlist = (RecyclerView) linearLayout.findViewById(R.id.rv_product);
            productRecyclerlist.setHasFixedSize(true);
            title.setText(featuredCategory.getCategory().getName().toUpperCase());
            if (preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE).equalsIgnoreCase(Language.PERSIAN)){
                title.setGravity(Gravity.RIGHT);
            }

            productRecyclerlist.setLayoutManager(getLinearLayoutManager());
            final ProductAdapter productAdapter = new ProductAdapter
                    (getActivity(), featuredCategory.getProduct(), ViewType.HOMEPAGE_VIEW);
            productRecyclerlist.setAdapter(productAdapter);

            viewAllBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("viewbutton clicked","viewbutton");
                    ProductService.productId = featuredCategory.getCategory().getId();
                    gotoProductListFragment(featuredCategory.getCategory());
                }
            });
           /* viewAllBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductService.productId = featuredCategory.getCategory().getId();
                    gotoProductListFragment();
                }
            });*/
            featuredCategoryContainerLayout.addView(linearLayout);

            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ProductService.productId = featuredCategory.getCategory().getId();
                    //gotoProductListFragment();
                }
            });



            productAdapter.SetOnItemClickListener(new ProductAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    ProductDetailFragment.productModel = productAdapter.products.get(position);
                    gotoProductDetailsFragment();
                }
            });

            if (featuredCategory.getSubCategory() != null && featuredCategory.getSubCategory().size() > 0) {
                subCatsBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addSubCats(subCatsBtn, featuredCategory.getSubCategory());
                    }
                });
            } else {
                subCatsBtn.setVisibility(View.GONE);
            }
        }
    }

    private void addSubCats(View view, final List<BaseProductModel> subCategories) {


        PopupMenu popupMenu = new PopupMenu(getActivity(), view);

        for (BaseProductModel subCat : subCategories) {
            popupMenu.getMenu().add(subCat.getName());
        }

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                for (BaseProductModel bpl : subCategories) {
                    if (bpl.getName().equals(menuItem.getTitle())) {
                        ProductService.productId = bpl.getId();
                        gotoProductListFragment(bpl);
                        break;
                    }
                }
                return true;
            }
        });
        popupMenu.show();


    }

    private void addAdapterOnclickListener(final HomePageProductAdapter homePageProductAdapter) {
        homePageProductAdapter.SetOnItemClickListener(new HomePageProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ProductService.productId = homePageProductAdapter.productsList.get(position).getId();
                gotoProductListFragment(homePageProductAdapter.productsList.get(position));
            }
        });
    }

    private void homepageProductClickListener(final HomePageProductAdapter homePageProductAdapter)

    {
        homePageProductAdapter.SetOnItemClickListener(new HomePageProductAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ProductDetailFragment.productModel = (ProductModel) homePageProductAdapter.productsList.get(position);
                gotoProductDetailsFragment();
            }
        });
    }

    private void gotoProductListFragment(BaseProductModel baseProductModel) {
        getFragmentManager().beginTransaction().replace
                (R.id.container, ProductListFragmentFor3_8.newInstance(baseProductModel.getName(), (int) baseProductModel.getId())).addToBackStack(null).commit();
    }

    private void gotoProductDetailsFragment() {
        getFragmentManager().beginTransaction().replace(R.id.container, new ProductDetailFragment()).
                addToBackStack(null).commit();

    }

    @Override
    public void onStart() {
        super.onStart();
        String SCREEN_NAME="Home Page";
        //pushAnalyticsEvent(SCREEN_NAME);
    }
}
