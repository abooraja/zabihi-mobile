package com.bs.ecommerce.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.TextView;

import com.bs.ecommerce.Constants;
import com.bs.ecommerce.R;
import com.bs.ecommerce.model.LoginResponse;
import com.bs.ecommerce.networking.NetworkUtil;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.fragment.BarCodeCaptureFragment;
import com.bs.ecommerce.ui.fragment.CartFragment;
import com.bs.ecommerce.ui.fragment.ContactusFragment;
import com.bs.ecommerce.ui.fragment.CustomerAddressesFragment;
import com.bs.ecommerce.ui.fragment.CustomerInfoFragment;
import com.bs.ecommerce.ui.fragment.CustomerOrdersFragment;
import com.bs.ecommerce.ui.fragment.HomePageFragment;
import com.bs.ecommerce.ui.fragment.LoginFragment;
import com.bs.ecommerce.ui.fragment.SearchFragment;
import com.bs.ecommerce.ui.fragment.Utility;
import com.bs.ecommerce.ui.fragment.WishlistFragment;
import com.bs.ecommerce.utils.Language;

import java.lang.reflect.Field;
import java.util.Locale;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import de.greenrobot.event.EventBus;

//import com.facebook.login.LoginManager;

/**
 *  .
 */
public class BaseActivity extends AppCompatActivity {
    public ActionBarDrawerToggle mDrawerToggle;
    public TextView ui_hot;


    public PreferenceService preferenceService;


    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        preferenceService = PreferenceService.getInstance();

        setLocale(false);

        //AndroidInjection.inject(this);

    }

    public void onEvent(LoginResponse response) {
        if (response.getToken() != null) {
            invalidateOptionsMenu();
        }
    }

    public void getBaseUrl() {
        if (preferenceService.GetPreferenceBooleanValue(PreferenceService.DO_USE_NEW_URL)) {
            Constants.BASE_URL = preferenceService.GetPreferenceValue
                    (PreferenceService.URL_PREFER_KEY);
        }
    }


    public void goMenuItemFragment(Fragment fragment) {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

    }

    public void goMenuItemFragmentifloggedIn(Fragment fragment) {
        if (!isLoggedIn())
            fragment = new LoginFragment();
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        changeMenuItemLoginAction(menu);
        setOrRefreshCurtMenuItem(menu);
        return true;
    }

    private void setOrRefreshCurtMenuItem(Menu menu) {
        final View menu_hotlist = menu.findItem(R.id.menu_cart).getActionView();
        ui_hot = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
        menu_hotlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.closeLeftDrawer();

                if (!(getSupportFragmentManager().findFragmentById(R.id.container) instanceof CartFragment))
                    goMenuItemFragment(new CartFragment());
            }
        });
        updateHotCount(Utility.cartCounter);
    }

    public void updateHotCount(final int badgeCount) {
        // badgeCount = new_hot_number;
        if (ui_hot == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (badgeCount == 0)
                    ui_hot.setVisibility(View.INVISIBLE);
                else {
                    ui_hot.setVisibility(View.VISIBLE);
                    ui_hot.setText(Long.toString(badgeCount));
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int resource = item.getItemId();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (resource == R.id.menu_cart) {
            goMenuItemFragment(new CartFragment());
            return true;
        } else if (resource == R.id.menu_item_login && !(fragment instanceof LoginFragment)) {
            if (isLoggedIn()) {
                logoutConfirmationDialog();
                item.setTitle(getString(R.string.login));
            } else {
                goMenuItemFragment(new LoginFragment());
            }
            return true;
        } else if (resource == R.id.menu_item_customer_info && !(fragment instanceof CustomerInfoFragment)) {

            goMenuItemFragmentifloggedIn(new CustomerInfoFragment());

            return true;
        } else if (resource == R.id.menu_item_addresses && !(fragment instanceof CustomerAddressesFragment)) {
            goMenuItemFragmentifloggedIn(new CustomerAddressesFragment());
            return true;
        } else if (resource == R.id.menu_item_orders && !(fragment instanceof CustomerOrdersFragment)) {
            goMenuItemFragmentifloggedIn(new CustomerOrdersFragment());
            return true;
        } else if (resource == R.id.menu_item_wishlist && !(fragment instanceof WishlistFragment)) {
            goMenuItemFragmentifloggedIn(new WishlistFragment());
            return true;
        } else if (resource == R.id.menu_search && !(fragment instanceof SearchFragment)) {
            goMenuItemFragment(new SearchFragment());
            return true;
        } else if (resource == R.id.menu_item_contact_us && !(fragment instanceof ContactusFragment)) {
            goMenuItemFragment(new ContactusFragment());
            return true;
        } else if (resource == R.id.menu_item_barcode_read && !(fragment instanceof BarCodeCaptureFragment)) {
            goMenuItemFragment(new BarCodeCaptureFragment());
            return true;
        /* } else if (resource == R.id.menu_item_change_url) {
            Intent intent = new Intent(this, BaseUrlChangeActivity.class);
            startActivity(intent);
            return true;
            */
        } 

        return super.onOptionsItemSelected(item);
    }


    public void changeMenuItemLoginAction(Menu menu) {
        if (isLoggedIn()) {
            menu.findItem(R.id.menu_item_login).setTitle(R.string.log_out);
        } else
            menu.findItem(R.id.menu_item_login).setTitle(R.string.sign_in);
    }

    public void makeActionOverflowMenuShown() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            Log.d("", e.getLocalizedMessage());
        }
    }

    public boolean isLoggedIn() {
        return preferenceService.GetPreferenceBooleanValue(PreferenceService.LOGGED_PREFER_KEY);
    }

    public void performLogout() {
        /*NetworkUtilities.token = "";*/
        NetworkUtil.setToken("");
        Utility.cartCounter = 0;
        preferenceService.SetPreferenceValue(PreferenceService.TOKEN_KEY, "");
        preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY, false);
//        LoginManager.getInstance().logOut();
        goMenuItemFragment(new HomePageFragment());
        invalidateOptionsMenu();
    }

    private void logoutConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);

        builder.setMessage(R.string.are_you_logout)
                .setTitle(R.string.log_out);

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                performLogout();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public View getActivityContentView() {
        return this.getWindow().getDecorView().findViewById(android.R.id.content);

    }

    public void setLocale(boolean recreate) {

        //PreferenceService2.getInstance().preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String preferredLanguage = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        //boolean shouldRecreate = getActivityContentView() != null;
        if (TextUtils.isEmpty(preferredLanguage)) {
            preferredLanguage = Language.ENGLISH;
            preferenceService.SetPreferenceValue(PreferenceService.CURRENT_LANGUAGE, preferredLanguage);
        }

        Locale current = Locale.getDefault();
        String currentLanguage = current.getLanguage();


        if (!currentLanguage.equalsIgnoreCase(preferredLanguage)) {
            Locale preferredLocale = new Locale(preferredLanguage);
            Locale.setDefault(preferredLocale);
            Configuration configuration = new Configuration();
            configuration.locale = preferredLocale;
            getBaseContext().getResources().updateConfiguration(configuration, getBaseContext().getResources().getDisplayMetrics());
            if (recreate) {
                recreate();
            }
        }
    }

}
