package com.bs.ecommerce.ui.adapter;

import com.bs.ecommerce.ui.fragment.BillingAddressFragment;
import com.bs.ecommerce.ui.fragment.ConfirmOrderFragment;
import com.bs.ecommerce.ui.fragment.PaymentInformationFragment;
import com.bs.ecommerce.ui.fragment.PaymentMethodFragment;
import com.bs.ecommerce.ui.fragment.ShippingAddressFragment;
import com.bs.ecommerce.ui.fragment.ShippingMethodFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 *
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    public static int adapterSize = 5;
    public String title[] = {"Billing", "Shipping", "Shipping Method",
            "Payment Method","Confirm Order", "Payment Infromation"};

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == FragmentClass.BillingAddress)
          fragment=new BillingAddressFragment();
       else if (position == FragmentClass.ShippingAddress)
            fragment=new ShippingAddressFragment();
        else if (position == FragmentClass.ShippingMethod)
            fragment=new ShippingMethodFragment();
        else if (position == FragmentClass.PaymentMethod)
            fragment=new PaymentMethodFragment();
        else if (position == FragmentClass.PaymentInfromation)
            fragment=new PaymentInformationFragment();

        else if (position == FragmentClass.ConfirmOrder)
            fragment=new ConfirmOrderFragment();

            return fragment;

    }

    @Override
    public int getCount() {
        return adapterSize;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }


}
