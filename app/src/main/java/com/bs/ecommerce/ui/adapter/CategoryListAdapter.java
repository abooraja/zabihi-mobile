package com.bs.ecommerce.ui.adapter;

import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.Category;
import com.bs.ecommerce.model.ProductService;
import com.bs.ecommerce.model.ViewType;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.ui.fragment.ProductListFragmentFor3_8;
import com.bs.ecommerce.ui.fragment.Utility;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Arif Islam on 23-Feb-17.
 */

public class CategoryListAdapter extends RecyclerView.Adapter {
    private List<Category> productList;
    private Context context;
    private OnItemClickListener productClickListener;
    private PreferenceService preferenceService;
    private Fragment fragment;

    public CategoryListAdapter(Context context, List<Category> productList, PreferenceService preferenceService, Fragment fragment) {
        this.context = context;
        this.productList = productList;
        this.preferenceService=preferenceService;
        this.fragment=fragment;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_custom, parent, false);
        return new ProductSummaryHolder(itemView);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder bindViewHolder, final int position) {

        Category productModel = productList.get(position);
        ProductSummaryHolder holder = (ProductSummaryHolder) bindViewHolder;
        holder.subName.setText(productModel.getName().toUpperCase());
        holder.customList.setAdapter(new SubCategoryListAdapter(context,productModel.getChildren(),preferenceService,fragment));
        holder.subName.setOnClickListener(new CategoryonClicklistener(productModel));
        holder.customList.setGroupIndicator(null);
    }

    @Override
    public int getItemViewType(int position) {
        return ViewType.LIST;
    }

    @Override
    public int getItemCount() {
        return productList == null ? 0 : productList.size();
    }

    private void gotoProductListPage(Category category) {
        Utility.closeLeftDrawer();

        fragment.getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        fragment.getFragmentManager().beginTransaction()
                .replace(R.id.container, ProductListFragmentFor3_8.newInstance(category.getName(), category.getId()))
                .addToBackStack(null)
                .commit();
    }


    public interface OnItemClickListener {
        void onItemClick(View view, Category category);
    }

    private class ProductSummaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView subName;
        ExpandableListView customList;

        ProductSummaryHolder(View itemView) {
            super(itemView);
            subName = (TextView) itemView.findViewById(R.id.subName);
            customList=(ExpandableListView)itemView.findViewById(R.id.customList);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (productClickListener != null) {
                productClickListener.onItemClick(v, productList.get(getAdapterPosition()));
            }
        }

    }

    ;

    private class CategoryonClicklistener implements View.OnClickListener
    {
        Category category;
        public  CategoryonClicklistener(Category category)
        {
            this.category=category;
        }
        @Override
        public void onClick(View v) {
            ProductService.productId=category.getId();

            gotoProductListPage(category);
        }
    }


}
