package com.bs.ecommerce.interface_pack;

/**
 *
 */
public interface ChangeTabEventListener {
    public void onTabChanged(int position);
}
