package com.bs.ecommerce;

import android.widget.RelativeLayout;

import com.bs.ecommerce.model.Category;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.bs.ecommerce.networking.response.CategoryResponse;
import com.bs.ecommerce.ui.fragment.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CategoryEvent {
    public static List<Category>rootCategories;
    public static List<Category>subCategories;
    public static List<Category>allCategories;


    public static void initialize()
    {
      rootCategories=new ArrayList<>();
       subCategories=new ArrayList<>();
        allCategories=new ArrayList<>();
    }
public static  List<Category> rootCategories(RelativeLayout layout)
    {

        if(rootCategories==null || rootCategories.size()==0) {
            RetroClient.getApi().getCategory().enqueue(new CustomCB<CategoryResponse>(layout));
        }

        return rootCategories;
    }

   public static ArrayList<Category>categoryList(int parentId)
    {
        ArrayList<Category>data=new ArrayList<>();
        for(Category category:subCategories)
           if(category.getParentCategoryId()==parentId)
               data.add(category);
        return  data;
    }

    public static ArrayList<Category>categoryList()
    {
        int parentId=0;
        ArrayList<Category>data=new ArrayList<>();
        for(Category category:allCategories)
        {
            if(category.getId()==Utility.categoryId)
            {
                parentId=category.getParentCategoryId();
                break;
            }
        }
        if(parentId!=0)
        for(Category category:allCategories)
            if(category.getParentCategoryId()== parentId)
                data.add(category);
        Utility.categoryId=parentId;
        return  data;
    }

    //   @Override
    public void saveData(List<Category> list) {
        rootCategories = new ArrayList<>();
        subCategories = new ArrayList<>();

        for (Category category : list)
            if (category.getParentCategoryId() == 0)
                rootCategories.add(category);
            else
                subCategories.add(category);
        allCategories = list;


    }

   /* public static ArrayList<String> categoryList(List<Category>categories)
    {
        ArrayList<String>data=new ArrayList<>();
        for(Category category:categories)
        data.add(category.getName());
        return  data;
    }
    public static ArrayList<String>categoryList(int parentId)
    {
        ArrayList<Category>data=new ArrayList<>();
        for(Category category:subCategories)
            data.add(category.getName());
        return  data;
    }*/
}
