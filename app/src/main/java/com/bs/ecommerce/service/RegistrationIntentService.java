package com.bs.ecommerce.service;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.util.Patterns;

import com.bs.ecommerce.R;
import com.bs.ecommerce.model.AppInitRequestResponse;
import com.bs.ecommerce.model.AppStartRequest;
import com.bs.ecommerce.model.AppThemeResponse;
import com.bs.ecommerce.networking.CustomCB;
import com.bs.ecommerce.networking.RetroClient;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.regex.Pattern;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import de.greenrobot.event.EventBus;


/**
 * .
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    PreferenceService preferenceService;


    public RegistrationIntentService() {
        super(TAG);
        preferenceService = PreferenceService.getInstance();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]
            //https://developer.clevertap.com/docs/find-your-gcm-sender-id-gcm-api-server-key
       //     if(!preferenceService.GetPreferenceBooleanValue(PreferenceService.SENT_TOKEN_TO_SERVER))
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_SenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);

            sendRegistrationToServer(token);

            subscribeTopics(token);
         // preferenceService.SetPreferenceValue(PreferenceService.SENT_TOKEN_TO_SERVER, true);
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            preferenceService.SetPreferenceValue(PreferenceService.SENT_TOKEN_TO_SERVER, false);

        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(PreferenceService.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {

        AppStartRequest appStartRequest=new AppStartRequest();
        appStartRequest.setDeviceTypeId(10);
        appStartRequest.setEmailAddress(getEmailAddress());
        appStartRequest.setSubscriptionId(token);
        EventBus.getDefault().register(this);

        RetroClient.getApi().initApp(appStartRequest).enqueue(new CustomCB<AppThemeResponse>());
    }

    public void onEvent(AppInitRequestResponse appInitRequestResponse) {

        preferenceService.SetPreferenceValue(PreferenceService.SENT_TOKEN_TO_SERVER, true);
        Log.i(TAG,"Registered" );

    }



    private String getEmailAddress() {

        String Email="N/A";
        AccountManager accountManager = AccountManager.get(this);
        if (getAccount(accountManager) != null) {
            return getAccount(accountManager).name;
        } else {
            Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            Account[] accounts = AccountManager.get(this).getAccounts();
            for (Account account : accounts) {
                if (emailPattern.matcher(account.name).matches()) {
                    return account.name;

                }
            }
        }
        return Email;
    }
    private  Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

}