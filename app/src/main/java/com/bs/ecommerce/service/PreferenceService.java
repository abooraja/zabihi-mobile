package com.bs.ecommerce.service;

import android.content.SharedPreferences;

/**
 * Created by MJV on 02/04/2019.
 */

public class PreferenceService {
    public static String SHARED_PREF_KEY="PREFER_KEY";
    public static String loggedInText="isLoggedIn";
    public static String LOGGED_PREFER_KEY="isLoggedIn";
    public static String TOKEN_KEY="token";
    public static String URL_PREFER_KEY="URL_CHANGE_KEY";
    public static String DO_USE_NEW_URL="URL_CHANGE_KEY_BOOLEAN";
    public static String APP_VERSION_CODE_KEY="VERSION_CODE";
    public static String SENT_TOKEN_TO_SERVER="SENT_TOKEN_TO_SERVER_KEY";
    public static String REGISTRATION_COMPLETE="REGISTRATION_COMPLETE_KEY";
    public static String taxShow="taxShow";
    public static String discuntShow="discuntShow";
    public static String CURRENT_LANGUAGE="current_language";
    public static String CURRENT_LANGUAGE_ID="current_language_id";
    private static PreferenceService ourInstance;//= new PreferenceService2();
    private SharedPreferences preferences;

    public PreferenceService(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public static PreferenceService getInstance() {
        return ourInstance;
    }

    public static void setInstance(PreferenceService instance) {
        ourInstance = instance;
    }

    public String GetPreferenceValue(String key)
    {

        return preferences.getString(key, null);
    }
    public int GetPreferenceIntValue(String key)
    {

        return preferences.getInt(key, -1);
    }

    public Boolean GetPreferenceBooleanValue(String key)
    {
        return preferences.getBoolean(key, false);
    }

    public void SetPreferenceValue(String key, String value)
    {
        preferences.edit().putString(key, value).commit();
    }

    public void SetPreferenceValue(String key, Boolean value)
    {
        preferences.edit().putBoolean(key, value).commit();
    }

    public void SetPreferenceValue(String key, int value)
    {
        preferences.edit().putInt(key, value).commit();
    }
}
