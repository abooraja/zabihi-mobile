package com.bs.ecommerce.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.bs.ecommerce.MainActivity;
import com.bs.ecommerce.R;
import com.google.android.gms.gcm.GcmListenerService;

import java.util.Random;

/**
 * .
 */
public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("alert");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        Log.v(TAG,"Message:"+from);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }


        sendNotification(data);
    }

    private void sendNotification(Bundle bundle) {
        Intent intent = new Intent(this, MainActivity.class);
        String message=bundle.getString("Message");
        String subject=bundle.getString("Subject");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("bundle",bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(subject)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Random random = new Random();
        int notID = random.nextInt(9999 - 1000) + 1000;
        if (Build.VERSION.SDK_INT < 16) {
            notificationManager.notify(notID /* ID of notification */, notificationBuilder.getNotification());
        } else {
            notificationManager.notify(notID, notificationBuilder.build());
        }
    }
}