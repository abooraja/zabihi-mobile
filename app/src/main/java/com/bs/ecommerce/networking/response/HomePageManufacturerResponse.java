package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.ManuFacturer;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class HomePageManufacturerResponse extends BaseResponse {
    private List<ManuFacturer> Data = new ArrayList<>();

    public List<ManuFacturer> getData() {
        return Data;
    }

    public void setData(List<ManuFacturer> data) {
        Data = data;
    }
}
