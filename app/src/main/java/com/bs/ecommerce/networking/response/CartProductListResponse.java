package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.CartProduct;
import com.bs.ecommerce.model.ProductAttribute;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.List;

/**
 *
 */
public class CartProductListResponse extends BaseResponse {
    private List<CartProduct>Items;
    private List<ProductAttribute>CheckoutAttributes;
    private com.bs.ecommerce.model.OrderReviewData OrderReviewData;
    private OrderTotalResponse OrderTotalResponseModel;
    private int Count;

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public List<ProductAttribute> getCheckoutAttributes() {
        return CheckoutAttributes;
    }

    public void setCheckoutAttributes(List<ProductAttribute> checkoutAttributes) {
        CheckoutAttributes = checkoutAttributes;
    }



    public com.bs.ecommerce.model.OrderReviewData getOrderReviewData() {
        return OrderReviewData;
    }

    public void setOrderReviewData(com.bs.ecommerce.model.OrderReviewData orderReviewData) {
        OrderReviewData = orderReviewData;
    }

    public List<CartProduct> getItems() {
        return Items;
    }

    public void setItems(List<CartProduct> items) {
        Items = items;
    }

    public OrderTotalResponse getOrderTotalResponseModel() {
        return OrderTotalResponseModel;
    }

    public void setOrderTotalResponseModel(OrderTotalResponse orderTotalResponseModel) {
        OrderTotalResponseModel = orderTotalResponseModel;
    }
}
