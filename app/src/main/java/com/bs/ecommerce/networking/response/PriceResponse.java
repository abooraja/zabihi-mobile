package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.networking.BaseResponse;

/**
 * .
 */
public class PriceResponse extends BaseResponse {
    private String Price;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }
}
