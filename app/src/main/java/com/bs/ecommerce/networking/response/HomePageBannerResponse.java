package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.ImageModel;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 *  on 11/9/2015.
 */
public class HomePageBannerResponse extends BaseResponse {
    private List<ImageModel> Data = new ArrayList<>();

    public List<ImageModel> getData() {
        return Data;
    }

    public void setData(List<ImageModel> data) {
        Data = data;
    }


}
