package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.Category;
import com.bs.ecommerce.networking.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 *
 */
public class CategoryResponse extends BaseResponse{
    private List<Category> Data;
    private int Count;
    @SerializedName("DisplayTaxInOrderSummary")
    private boolean displayTaxInOrderSummary;
    @SerializedName("ShowDiscountBox")
    private boolean showDiscountBox;

    public List<Category> getData() {
        return Data;
    }

    public void setData(List<Category> data) {
        this.Data = data;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public boolean isDisplayTaxInOrderSummary() {
        return displayTaxInOrderSummary;
    }

    public void setDisplayTaxInOrderSummary(boolean displayTaxInOrderSummary) {
        this.displayTaxInOrderSummary = displayTaxInOrderSummary;
    }

    public boolean isShowDiscountBox() {
        return showDiscountBox;
    }

    public void setShowDiscountBox(boolean showDiscountBox) {
        this.showDiscountBox = showDiscountBox;
    }
}
