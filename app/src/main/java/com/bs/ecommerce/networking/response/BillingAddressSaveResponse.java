package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.networking.BaseResponse;

/**
 *
 */
public class BillingAddressSaveResponse extends BaseResponse {
    private boolean Data;

    public boolean isData() {
        return Data;
    }

    public void setData(boolean data) {
        Data = data;
    }
}
