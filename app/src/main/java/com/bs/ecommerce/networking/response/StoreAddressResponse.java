package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.StoreDM;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class StoreAddressResponse extends BaseResponse {

private List<StoreDM> PickupPoints=new ArrayList<>();


    public List<StoreDM> getPickupPoints() {
        return PickupPoints;
    }

    public void setPickupPoints(List<StoreDM> pickupPoints) {
        PickupPoints = pickupPoints;
    }
}
