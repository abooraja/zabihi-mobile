package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.ShippingMethod;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.List;

/**
 *
 */
public class ShippingMethodRetrievalResponse extends BaseResponse{
    private List<ShippingMethod>ShippingMethods;
    private boolean NotifyCustomerAboutShippingFromMultipleLocations;


    public boolean isNotifyCustomerAboutShippingFromMultipleLocations() {
        return NotifyCustomerAboutShippingFromMultipleLocations;
    }

    public void setNotifyCustomerAboutShippingFromMultipleLocations(boolean notifyCustomerAboutShippingFromMultipleLocations) {
        NotifyCustomerAboutShippingFromMultipleLocations = notifyCustomerAboutShippingFromMultipleLocations;
    }


    public List<ShippingMethod> getShippingMethods() {
        return ShippingMethods;
    }

    public void setShippingMethods(List<ShippingMethod> shippingMethods) {
        ShippingMethods = shippingMethods;
    }


}
