package com.bs.ecommerce.networking;

import android.content.Context;

import com.bs.ecommerce.DeviceUniqueIdentifier;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bs156 on 09-Dec-16.
 */

public class NetworkUtil {
    private static String deviceId = "";
    private static String token = "";

    static String getDeviceId() {
        if (deviceId.isEmpty()) {
            deviceId = DeviceId.get();
        }
        return deviceId;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        NetworkUtil.token = token;
    }

    public static Map<String, String> getHeaders() {
        Map<String, String> headerMap = new HashMap<>();
        if (deviceId.isEmpty()) {
            deviceId = DeviceId.get();
        }
        headerMap.put("DeviceId", deviceId);

        if (token != null && !token.isEmpty()) {
            headerMap.put("Token", token);
        }

        return headerMap;
    }
}
