package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.ProductModel;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class HomePageProductResponse extends BaseResponse {
    private List<ProductModel> Data = new ArrayList<>();

    public List<ProductModel> getData() {
        return Data;
    }

    public void setData(List<ProductModel> data) {
        Data = data;
    }

}
