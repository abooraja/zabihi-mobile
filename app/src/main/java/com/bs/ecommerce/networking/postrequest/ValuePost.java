package com.bs.ecommerce.networking.postrequest;

/**
 *
 */
public class ValuePost {

    private String value;
    public ValuePost()
    {

    }
    public ValuePost(String value)
    {
        this.value=value;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
