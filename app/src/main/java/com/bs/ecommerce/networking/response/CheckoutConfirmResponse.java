package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.networking.BaseResponse;

/**
 *
 */
public class CheckoutConfirmResponse extends BaseResponse{

    private long OrderId;
    private com.bs.ecommerce.model.PayPal PayPal;
    private int Data;
    private boolean CompleteOrder;
    private int PaymentType;

    public int getData( ) {
        return Data;
    }

    public void setData(int data) {
        Data = data;
    }

    public boolean isCompleteOrder() {
        return CompleteOrder;
    }

    public void setCompleteOrder(boolean completeOrder) {
        CompleteOrder = completeOrder;
    }

    public com.bs.ecommerce.model.PayPal getPayPal() {
        return PayPal;
    }

    public void setPayPal(com.bs.ecommerce.model.PayPal payPal) {
        PayPal = payPal;
    }



    public long getOrderId() {
        return OrderId;
    }

    public void setOrderId(long orderId) {
        OrderId = orderId;
    }

    public int getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(int paymentType) {
        PaymentType = paymentType;
    }
}
