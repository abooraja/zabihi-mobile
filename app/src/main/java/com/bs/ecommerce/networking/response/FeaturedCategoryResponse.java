package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.FeaturedCategory;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.List;

/**
 * .
 */
public class FeaturedCategoryResponse  extends BaseResponse{
    private List<FeaturedCategory> Data;

    public List<FeaturedCategory> getData() {
        return Data;
    }

    public void setData(List<FeaturedCategory> data) {
        Data = data;
    }
}