package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.ProductDetail;
import com.bs.ecommerce.networking.BaseResponse;

/**
 * .
 */
public class ProductDetailResponse extends BaseResponse {
    private ProductDetail Data;

    public ProductDetail getData() {
        return Data;
    }

    public void setData(ProductDetail data) {
        Data = data;
    }
}
