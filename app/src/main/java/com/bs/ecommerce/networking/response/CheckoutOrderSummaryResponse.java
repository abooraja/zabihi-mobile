package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.networking.BaseResponse;

/**
 * .
 */
public class CheckoutOrderSummaryResponse extends BaseResponse {
    private  OrderTotalResponse OrderTotalModel;
    private CartProductListResponse ShoppingCartModel ;

    public OrderTotalResponse getOrderTotalModel() {
        return OrderTotalModel;
    }

    public void setOrderTotalModel(OrderTotalResponse orderTotalModel) {
        OrderTotalModel = orderTotalModel;
    }

    public CartProductListResponse getShoppingCartModel() {
        return ShoppingCartModel;
    }

    public void setShoppingCartModel(CartProductListResponse shoppingCartModel) {
        ShoppingCartModel = shoppingCartModel;
    }


}
