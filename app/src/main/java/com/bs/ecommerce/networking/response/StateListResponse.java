package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.AvailableState;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.List;

/**
 *
 */
public class StateListResponse extends BaseResponse {
    List<AvailableState> Data;

    public List<AvailableState> getData() {
        return Data;
    }

    public void setData(List<AvailableState> data) {
        Data = data;
    }
}
