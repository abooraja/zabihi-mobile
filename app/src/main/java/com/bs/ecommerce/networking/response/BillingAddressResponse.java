package com.bs.ecommerce.networking.response;

import com.bs.ecommerce.model.BillingAddress;
import com.bs.ecommerce.networking.BaseResponse;

import java.util.List;

/**
 *
 */
public class BillingAddressResponse extends BaseResponse {

    private List<BillingAddress> ExistingAddresses;
    private BillingAddress NewAddress;
    private boolean NewAddressPreselected;

    public List<BillingAddress> getExistingAddresses() {
        return ExistingAddresses;
    }

    public void setExistingAddresses(List<BillingAddress> existingAddresses) {
        ExistingAddresses = existingAddresses;
    }

    public boolean isNewAddressPreselected() {
        return NewAddressPreselected;
    }

    public void setNewAddressPreselected(boolean newAddressPreselected) {
        NewAddressPreselected = newAddressPreselected;
    }

    public BillingAddress getNewAddress() {
        return NewAddress;
    }

    public void setNewAddress(BillingAddress newAddress) {
        NewAddress = newAddress;
    }

}
