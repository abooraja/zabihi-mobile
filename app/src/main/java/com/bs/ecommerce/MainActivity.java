package com.bs.ecommerce;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.bs.ecommerce.model.ProductModel;
import com.bs.ecommerce.model.ProductService;
import com.bs.ecommerce.networking.NetworkUtil;
import com.bs.ecommerce.service.PreferenceService;
import com.bs.ecommerce.service.RegistrationIntentService;
import com.bs.ecommerce.ui.activity.BaseActivity;
import com.bs.ecommerce.ui.fragment.HomePageFragment;
import com.bs.ecommerce.ui.fragment.ProductDetailFragment;
import com.bs.ecommerce.ui.fragment.ProductListFragmentFor3_8;
import com.bs.ecommerce.utils.Language;

import java.lang.reflect.Method;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

//@ContentView(R.layout.activity_main)
public class MainActivity extends BaseActivity implements View.OnClickListener {

    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    public static MainActivity self;
    @BindView(R.id.drawer_layout)
    public DrawerLayout drawerLayout;
    Toolbar toolbar;
    private boolean doubleBackToExitPressedOnce = false;
    private long mBackPressed;


//    PreferenceService preferenceService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/iransan.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        /*NetworkUtilities.context = this;*/
        self = this;
        setToolbar();
        getBaseUrl();
        if (savedInstanceState == null)
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.container, new HomePageFragment()).commit();

        setUp();

        resetAuthenticatioToken();
        startGCmService();
        checkNotificationBundle();



    }

    private void checkNotificationBundle()
    {
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {
            String  NotificationTypeId=bundle.getString("NotificationTypeId");
            String itemId=bundle.getString("ItemId");

            if(NotificationTypeId=="1")
            {
                ProductModel productModel=new ProductModel();
                productModel.setId(Integer.parseInt(itemId));
                productModel.setName("");
                ProductDetailFragment.productModel=productModel;
                gotoFragment(new ProductDetailFragment());
            }
            else if(NotificationTypeId=="2")
            {
                ProductService.productId=Integer.parseInt(itemId);
                gotoFragment(ProductListFragmentFor3_8.newInstance("", Integer.parseInt(itemId)));

            }
        }

    }

    private void gotoFragment(Fragment fragment)
    {
        getSupportFragmentManager().beginTransaction().
                replace(R.id.container, fragment).commit();
    }

    private void startGCmService()
    {

        if(!getApplicationInfo().packageName.equals("com.bs.ecommerce.obeezi.ng")) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }


    private void setToolbar() {
        toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.app_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

    }

    private void resetAuthenticatioToken()
    {
        /*NetworkUtilities.token="";*/
        NetworkUtil.setToken("");
        if(preferenceService.GetPreferenceIntValue(PreferenceService.APP_VERSION_CODE_KEY)!=getApplicationVersionCode())
        {
            preferenceService.SetPreferenceValue(PreferenceService.APP_VERSION_CODE_KEY,getApplicationVersionCode());
            preferenceService.SetPreferenceValue(PreferenceService.LOGGED_PREFER_KEY,false);

        }
        if (preferenceService.GetPreferenceBooleanValue(preferenceService.LOGGED_PREFER_KEY)) {
            /*NetworkUtilities.token = preferenceService
                    .GetPreferenceValue(preferenceService.TOKEN_KEY);*/
            NetworkUtil.setToken(preferenceService
                    .GetPreferenceValue(preferenceService.TOKEN_KEY));
        }
    }
    private int getApplicationVersionCode()
    {
        int versionCode=-1;
        PackageManager manager = getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(
                    getPackageName(), -1);
            versionCode  = info.versionCode;
            return versionCode;
        }
        catch (Exception exception)
        {

        }
        return versionCode;
    }

    public void setUp() {
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        ) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                invalidateOptionsMenu();
                syncState();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
                syncState();
            }

        };

        drawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.syncState();
        toolbar.setNavigationOnClickListener(this);
        listeningFragmentTransaction();
        makeActionOverflowMenuShown();


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            handleAppExit();
        } else
            super.onBackPressed();

    }

    public void handleAppExit() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(getBaseContext(), R.string.back_button_click_msg, Toast.LENGTH_SHORT).show();
        }

        mBackPressed = System.currentTimeMillis();
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.back_button_click_msg, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void popBackstack() {
        FragmentManager fm = getSupportFragmentManager(); // or 'getSupportFragmentManager();'
        int count = fm.getBackStackEntryCount();
        for (int i = 0; i < count; ++i) {
            fm.popBackStackImmediate();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {

        if (menu != null) {
            if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
                try {
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {
                    Log.e(getClass().getSimpleName(), "onMenuOpened...unable to set icons for overflow menu", e);
                }
            }
        }
        return super.onPrepareOptionsPanel(view, menu);
    }


    @Override
    public void onClick(View v) {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp() {
        Log.d("clicking", "yes");
        if (getSupportActionBar() != null) {
            if (shouldOpenDrawer()) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                setArrowIconInDrawer();
                onBackPressed();
                shouldOpenDrawer();
            } else {
                mDrawerToggle.setDrawerIndicatorEnabled(true);
                // getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            }
        }
    }

    public boolean shouldOpenDrawer() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            mDrawerToggle.setDrawerIndicatorEnabled(true);

            return true;

        } else {
            return false;
        }


    }

    public void listeningFragmentTransaction() {
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    setArrowIconInDrawer();
                } else {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                    setAnimationOndrwerIcon();

                }
            }
        });
    }

//    public void setArrowIconInDrawer() {
//        mDrawerToggle.setDrawerIndicatorEnabled(false);
//        setUparrowColor();
//    }

    public void closeDrawer() {
        drawerLayout.closeDrawers();
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
        setUp();

    }

    private void setUparrowColor()
    {
        final Drawable upArrow = ContextCompat.getDrawable(this,R.drawable.ic_action_nav_arrow_back);
        upArrow.setColorFilter(ContextCompat.getColor(this,R.color.colorUparow), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    private void setAnimationOndrwerIcon() {
        ValueAnimator anim = ValueAnimator.ofFloat(1, 0);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                mDrawerToggle.onDrawerSlide(drawerLayout, slideOffset);
            }
        });
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(800);
        anim.start();

    }

    public void setArrowIconInDrawer() {
        mDrawerToggle.setDrawerIndicatorEnabled(false);

        getSupportActionBar().setHomeAsUpIndicator(getNavIconResId());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setDrawerIcon() {
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
    }

    protected int getNavIconResId() {
        String language = preferenceService.GetPreferenceValue(PreferenceService.CURRENT_LANGUAGE);
        if (language.equals(Language.PERSIAN)) {
            return R.drawable.ic_action_nav_arrow_back_inverted;
        } else {
            return R.drawable.ic_action_nav_arrow_back;
        }
    }

    public void displayHomeIcon(boolean displayHomeIcon) {
        if (getSupportActionBar() != null) {
            if (displayHomeIcon) {
                setArrowIconInDrawer();
            } else {
                setDrawerIcon();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



}
