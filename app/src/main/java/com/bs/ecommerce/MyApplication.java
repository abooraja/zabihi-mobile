package com.bs.ecommerce;

import android.content.Context;
import android.preference.PreferenceManager;

import com.bs.ecommerce.service.PreferenceService;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;

//import roboguice.RoboGuice;

/**
 *
 */
public class MyApplication extends MultiDexApplication {
    //Logging TAG
    private static final String TAG = "MyApp";
    public static int GENERAL_TRACKER = 0;

    static {
        //RoboGuice.setUseAnnotationDatabases(false);
    }

    PreferenceService preferenceService;
    private String PROPERTY_ID;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this); // install multidex
    }

    @Override
    public void onCreate() {
        super.onCreate();


        PreferenceService.setInstance(new PreferenceService(PreferenceManager.getDefaultSharedPreferences(this)));
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);
//        SingletonComponent singletonComponent = DaggerSingletonComponent.builder()
//                .preferenceService(new PreferenceService())
//                //.contextModule(ContextModule.this)
//                .build();

        /*if(preferenceService.GetPreferenceBooleanValue(PreferenceService.DO_USE_NEW_URL))
        {
            Constants.BASE_URL=preferenceService.GetPreferenceValue
                    (PreferenceService.URL_PREFER_KEY);
        }*/
    }

    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

}
