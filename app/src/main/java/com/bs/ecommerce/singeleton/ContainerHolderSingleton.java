package com.bs.ecommerce.singeleton;


/**
 *
 */
public class ContainerHolderSingleton {

    /**
     * Utility class; don't instantiate.
     */
    private ContainerHolderSingleton() {
    }

}