package com.bs.ecommerce.utils;

import android.app.Activity;
import android.content.Context;
import android.app.Fragment;
import com.bs.ecommerce.R;
import com.bs.ecommerce.ui.fragment.BaseFragment;

/**
 * Created by bs-110 on 12/18/2015.
 */
public class AppUtils {
    public static String getNullSafeString(String string) {
        return string == null ? "" : string;
    }
}
