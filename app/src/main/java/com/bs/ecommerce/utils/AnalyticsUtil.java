package com.bs.ecommerce.utils;

/**
 * .
 */
public class AnalyticsUtil {

    public static String ANALYTICAL_EVENT="screenViews";
    public static String ANALYTICAL_SCREEN_VARIABLE_NAME="screen";
    public static String ANALYTICAL_OPEN_SCREEN_EVENT_NAME="openScreen";
}
