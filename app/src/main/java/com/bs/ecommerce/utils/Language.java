package com.bs.ecommerce.utils;

/**
 * Created by BS62 on 26-Oct-16.
 */

public class Language {
    public static final String ENGLISH = "en";
    public static final String PERSIAN = "fa";
    public static final String ARABIC = "ar";
    public static final String ITALIAN = "it";
}
