# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in E:\ADT\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#
#-ignorewarnings
#-dontwarn sun.misc.Unsafe
#-dontwarn javax.annotation.Nullable
#-dontwarn javax.annotation.ParametersAreNonnullByDefault
#-dontwarn javax.annotation.CheckReturnValue
#-dontwarn javax.annotation.CheckForNull
#-dontwarn javax.annotation.concurrent.GuardedBy
#-dontwarn javax.annotation.concurrent.Immutable
#-dontwarn javax.annotation.concurrent.ThreadSafe
#-dontwarn javax.annotation.concurrent.NotThreadSafe

-dontwarn roboguice.**
-dontwarn org.roboguice.**
-keep class roboguice.** { *; }
-keep class org.roboguice.** { *; }

#-keep public class com.** {public *; }

-keep class com.android.** { *; }
#-keep class com.squareup.** { *; }
#-keep class com.flipboard.** { *; }
#-keep class com.pnikosis.** { *; }
#-keep class com.bs.** {public protected private *; }
-keep class com.bs.ecommerce.networking.** {  *; }
-keep class com.bs.ecommerce.model.** { *; }
-keep class com.bs.ecommerce.service.** {*; }
-keep class com.bs.ecommerce.utils.** { *; }
-keep class com.bs.ecommerce.barcode.** {public *; }
-keep class com.bs.ecommerce.ui.** {public protected *; }
#-keep class com.github.** { *; }
#-keep class com.daimajia.** { *; }
#-keep class com.nineoldandroids.** { *; }
#-keep class com.commonsware.** { *; }
#-keep class com.marshalchen.** { *; }
#-keep class com.malinskiy.** { *; }
#-keep class com.daimajia.** { *; }
#-keep class com.flipboard.** { *; }
#-keep class com.daimajia.** { *; }

#-keepattributes *Annotation*,Signature
#-keep class com.google.inject.Binder
#-keep public class com.google.inject.Inject
##keeps all fields and Constructors with #Inject
#-keepclassmembers,allowobfuscation class * {
##com.google.inject.Inject <fields>;
##com.google.inject.Inject <init>(...);
#}
#
##-dontwarn com.fasterxml.**
#-dontwarn okio.**
-keep class okio.**{*;}
#-dontwarn retrofit2.**
-keep class retrofit2.**
#-dontwarn retrofit2.Platform$Java8
#
##-dontwarn org.codehaus.**
#-keep class org.codehaus.**
##-dontwarn java.nio.**
#-keep class java.nio.**
##-dontwarn java.lang.invoke.**
#-keep class java.lang.invoke.**
#
##-dontwarn com.google.android.gms.**
##-dontwarn com.kount.**
#
#-keep class com.google.android.gms.** { *; }
#
#-keepnames class com.fasterxml.jackson.databind.** { *; }
##-dontwarn com.fasterxml.jackson.databind.*
#-keepattributes InnerClasses
#
#-keep class org.bouncycastle.** { *; }
#-keepnames class org.bouncycastle.* { *; }
##-dontwarn org.bouncycastle.*
#
#-keep class io.jsonwebtoken.** { *; }
#-keepnames class io.jsonwebtoken.* { *; }
#-keepnames interface io.jsonwebtoken.* { *; }
#
##-dontwarn javax.xml.bind.DatatypeConverter
#-keep class javax.xml.bind.DatatypeConverter
##-dontwarn io.jsonwebtoken.impl.Base64Codec
#-keep class io.jsonwebtoken.impl.Base64Codec
#
#-keepnames class com.fasterxml.jackson.** { * ; }
#-keepnames interface com.fasterxml.jackson.** { *; }
#
##-dontwarn com.malinskiy.superrecyclerview.SwipeDismissRecyclerViewTouchListener*
#-keep class com.malinskiy.superrecyclerview.SwipeDismissRecyclerViewTouchListener*
#-ignorewarnings

#-keep class com.bs.ecommerce.,com.bs.ecommerce.*,com.bs.ecommerce.**,com.bs.ecommerce.*** {
#    public private *;
#}

#
#-keep class nop.bs.ecommerce.model.AdvanceSearchSpinnerOption {
#     public private *;
# }
#
#-keep class nop.bs.ecommerce.model.FilterAttribute {
#  public private *;
#}
#
#-keep class nop.bs.ecommerce.model.PriceRange {
#   public private *;
#}
#
#-keep class nop.bs.ecommerce.model.ProductFilterItem {
#   public private *;
#}
#
#-keep class nop.bs.ecommerce.model.ProductDetail {
#   public private *;
#}
#
#-keep class nop.bs.ecommerce.model.ProductSpecification {
#   public private *;
#}
#
#-keep class nop.bs.ecommerce.model.Quantity {
#  public private *;
#}
#
#-keep class nop.bs.ecommerce.model.Search{
#     public private *;
#}
#
#-keep class nop.bs.ecommerce.model.StoreDM{
#     public private *;
# }
#
#-keep class nop.bs.ecommerce.model.ConfirmAutorizeDotNetCheckoutResponse{
#     public private *;
# }

-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**

-dontwarn com.malinskiy.superrecyclerview.SwipeDismissRecyclerViewTouchListener*

-dontwarn com.squareup.okhttp.**

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8

-keep class org.bouncycastle.** { *; }
-keepnames class org.bouncycastle.** { *; }
-dontwarn org.bouncycastle.**

-keep class com.marshalchen.** { *; }
-keepnames class com.marshalchen.** { *; }
-dontwarn com.marshalchen.**

-keep class io.jsonwebtoken.** { *; }
-keepnames class io.jsonwebtoken.** { *; }
-dontwarn io.jsonwebtoken.**

# Ignoring all of the external "org" libraries
# (for example org.apache & org.jackson)
-keep class org.** { *; }
-dontwarn org.**

#-keep class com.** { *; }
#-dontwarn com.*

-keep class nop.bs.ecommerce.model,nop.bs.ecommerce.model.*,nop.bs.ecommerce.model.** {
    public private *;
}

-keep class nop.bs.ecommerce.networking,nop.bs.ecommerce.networking.*,nop.bs.ecommerce.networking.** {
    public private *;
}

#-keep class nop.bs.ecommerce.ui.activity.MotherActivity, nop.bs.ecommerce.ui.activity.SplashScreenActivity, nop.bs.ecommerce.ui.activity.AuthorizeDotNetPaymentMethodActivity, nop.bs.ecommerce.ui.activity.BaseUrlChangeActivity,FullScreenImageActivity {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.adapter.ProductListAdapter , nop.bs.ecommerce.ui.adapter.SearchAdapter , nop.bs.ecommerce.ui.adapter.SubCategoryListAdapter ,nop.bs.ecommerce.ui.adapter.UlimateViewAdapter, nop.bs.ecommerce.ui.adapter.WishlistAdapter, nop.bs.ecommerce.ui.adapter.CartAdapter, nop.bs.ecommerce.ui.adapter.CheckoutOrderProductAdapter, nop.bs.ecommerce.ui.adapter.DetailsSliderAdapter, nop.bs.ecommerce.ui.adapter.ExpandableListAdapter {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.adapter.FeaturedProductAdapter, nop.bs.ecommerce.ui.adapter.HomePageProductAdapter ,nop.bs.ecommerce.ui.adapter.NavDrawerExpListAdapterB, nop.bs.ecommerce.ui.adapter.ProductAdapter{
#    public private *;
#}
#    -keep class nop.bs.ecommerce.ui.views.CheckoutAttributeView {
#        public private *;
#    }
#    -keep class nop.bs.ecommerce.ui.customview.RadioGridGroupforReyMaterial {
#        public private *;
#    }
#-keep class nop.bs.ecommerce.ui.fragment.ProductListFragmentFor3_8, nop.bs.ecommerce.ui.fragment.ProductListFragmentOptional, nop.bs.ecommerce.ui.fragment.ProductsListFragment, nop.bs.ecommerce.ui.fragment.BaseFragment, nop.bs.ecommerce.ui.CategoryFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.CustomerInfoFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.CustomerOrderDetailsFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.FilterFragmentFor3_8 {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.HomePageFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.LoginFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.NavigationDrawerFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.PasswordChangeFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.PaymentMethodFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.ProductDetailFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.ShippingMethodFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.CustomerOrdersFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.ProductListCustomFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.ProductListFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.WishlistFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.BarCodeCaptureFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.CategoryNewFragment {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment.CartFragment {
#    public private *;
#}
#    -keep class nop.bs.ecommerce.ui.views.ProductAttributeViews{
#        public private *;
#    }
#    -keep class nop.bs.ecommerce.ui.views.MethodSelctionProcess{
#        public private *;
#    }
#-keep class nop.bs.ecommerce.networking.response.ProductDetailResponse {
#    public private *;
#}
#-keep class nop.bs.ecommerce.service, nop.bs.ecommerce.service.*, nop.bs.ecommerce.service.**, nop.bs.ecommerce.service.*** {
#    public private *;
#}
#-keep class nop.bs.ecommerce.utils, nop.bs.ecommerce.utils.*, nop.bs.ecommerce.utils.**, nop.bs.ecommerce.utils.*** {
#    public private *;
#}

#-keep class nop.bs.ecommerce.ui.adapter, nop.bs.ecommerce.ui.adapter.*, nop.bs.ecommerce.ui.adapter.** {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.fragment, nop.bs.ecommerce.ui.fragment.*, nop.bs.ecommerce.ui.fragment.** {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.customview, nop.bs.ecommerce.ui.customview.*, nop.bs.ecommerce.ui.customview.** {
#    public private *;
#}
#-keep class nop.bs.ecommerce.ui.views, nop.bs.ecommerce.ui.views.*, nop.bs.ecommerce.ui.views.** {
#    public private *;
#}
#-keep class nop.bs.ecommerce.utils, nop.bs.ecommerce.utils.*, nop.bs.ecommerce.utils.** {
#    public private *;
#}

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}
-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }


#-keep class nop.bs.ecommerce.ui.fragment.GuestCheckoutFragment {
#    public private *;
#}
